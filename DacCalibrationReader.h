#ifndef DAC_CALIBRATION_READER_H
#define DAC_CALIBRATION_READER_H

#include <iostream>
#include <string>
#include <vector>
#include <tuple>

class DacCalibrationReader
{
public:
	DacCalibrationReader();
	DacCalibrationReader(const std::string& filename);
	~DacCalibrationReader();
	
	bool LoadDacCalibration(const std::string& filename);
	
	bool HasDacCalibration(int dacCode) const;
	
	double DacVoltage(int dacCode, int dacValue) const;
	double DacVoltageError(int dacCode, int dacValue) const;
	
	bool   HasThresholdCalibration() const;
	
	double ThresholdVoltage(int coarse, int fine) const;
	double ThresholdVoltageError(int coarse, int fine) const;
	
protected:
	std::vector<std::vector<std::tuple<double, double> >*> data;
	std::vector<std::tuple<double, double>>* threshold;
};

#endif //DAC_CALIBRATION_READER_H

