#!/bin/bash
rf=${1:-""}

[[ -z "$rf" ]] && [[ -e "tpdsRes.txt" ]] && rm -i tpdsRes.txt

attempt=0
result=0

while true
do
	./tpDelayScan "$rf"
	result=$?
	
	if [[ -e "measurementStartedFlag" ]]; then attempt=0; rm measurementStartedFlag; fi
	if [[ $result -eq 0 ]]; then break; fi
	
	[[ -e "tpdsRes.txt" ]] && rf="tpdsRes.txt"
	
	if [[ $attempt -eq 5 ]]; then break; fi
	
	# Revive communication with the CERN board
	spidrreset 192.168.1.10
	Tpx3daq -m -i 1 -t 1 -p -1 -s HackToReviveCernBoard
	rm /localstore/TPX3/DATA/CHIP0/Test/HackToReviveCernBoard*.dat
	rm /localstore/TPX3/DATA/CHIP1/Test/HackToReviveCernBoard*.dat
	((attempt=attempt+1))
done



