#include "DacCalibrationReader.h"

#include "tpx3defs.h"
#include "tpx3dacsdescr.h"

#include "TFile.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"

using namespace std;

DacCalibrationReader::DacCalibrationReader():
data(TPX3_DAC_COUNT_TO_SET, nullptr),
threshold(nullptr)
{

}

DacCalibrationReader::DacCalibrationReader(const string& filename):
DacCalibrationReader()
{
	LoadDacCalibration(filename);
}

DacCalibrationReader::~DacCalibrationReader()
{
	for(auto& p : data)
		if(p)
			delete p;
	
	if(threshold)
		delete threshold;
}

bool DacCalibrationReader::LoadDacCalibration(const string& filename)
{
	TDirectory* curDir = gDirectory->CurrentDirectory();
	TFile file(filename.c_str(), "READ");
	
	if(!file.IsOpen())
		return false;
	
	TTreeReader treeRdr("dacCalibration");
	
	TTreeReaderValue<int32_t> codeRdr   (treeRdr, "code"   );
	TTreeReaderValue<int32_t> valueRdr  (treeRdr, "value"  );
	TTreeReaderValue<double>  voltageRdr(treeRdr, "voltage");
	TTreeReaderValue<double>  errorRdr  (treeRdr, "error"  );
	
	while(treeRdr.Next())
	{
		auto& dacData = data[*codeRdr - 1];
		
		if(!dacData)
			dacData = new vector<tuple<double, double> >(1 << TPX3_DAC_TABLE[*codeRdr - 1].bits, tuple<double, double>{0., 0.});
		
		dacData->at(*valueRdr) = make_tuple(*voltageRdr, *errorRdr);
	}
	
	if(file.Get<TTree>("thrCalibration"))
	{
		TTreeReader treeRdr("thrCalibration");
		
		TTreeReaderValue<int32_t> coarseRdr (treeRdr, "coarse" );
		TTreeReaderValue<int32_t> fineRdr   (treeRdr, "fine"   );
		TTreeReaderValue<double>  voltageRdr(treeRdr, "voltage");
		TTreeReaderValue<double>  errorRdr  (treeRdr, "error"  );
		
		threshold = new vector<tuple<double, double>>(1<<14, make_tuple(0., 0.));
		
		while(treeRdr.Next())
			threshold->at(*coarseRdr << 9 | *fineRdr) = make_tuple(*voltageRdr, *errorRdr);
	}
	
	file.Close();
	curDir->cd();
	return true;
}

bool DacCalibrationReader::HasDacCalibration(int dacCode) const
{
	return dacCode >= 1 && dacCode <= TPX3_DAC_COUNT_TO_SET && data[dacCode];
}

double DacCalibrationReader::DacVoltage(int dacCode, int dacValue) const
{
	return std::get<0>(data[dacCode-1]->at(dacValue)); 
}

double DacCalibrationReader::DacVoltageError(int dacCode, int dacValue) const
{
	return std::get<1>(data[dacCode-1]->at(dacValue)); 
}


bool DacCalibrationReader::HasThresholdCalibration() const
{
	return threshold;
}

double DacCalibrationReader::ThresholdVoltage(int coarse, int fine) const
{
	return get<0>(threshold->at(coarse << 9 | fine));
}

double DacCalibrationReader::ThresholdVoltageError(int coarse, int fine) const
{
	return get<1>(threshold->at(coarse << 9 | fine));
}