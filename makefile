SPIDR_TPX3_INC = /localstore/TPX3/SPIDR/software/trunk/SpidrTpx3Lib
SPIDR_TPX3_LIB = /localstore/TPX3/SPIDR/software/trunk/Release
STPX3DT_INC = /localstore/TPX3/user/kevinh/software/SpidrTpx3DataTools/inc
STPX3DT_LIB = /localstore/TPX3/user/kevinh/software/SpidrTpx3DataTools/lib
CONRAD_INC = /localstore/TPX3/user/kevinh/software/conrad
CONRAD_LIB = /localstore/TPX3/user/kevinh/software/conrad
NI_LIBS = /usr/lib/x86_64-linux-gnu
NI_INC = /usr/include/ni-visa

LINK.o = $(LINK.cc)
CXXFLAGS = `root-config --cflags` -std=c++11 -Og -g -Wall -Wfatal-errors -m64 -I $(SPIDR_TPX3_INC) -I $(STPX3DT_INC) -I $(NI_INC) -I $(CONRAD_INC)
LDFLAGS  = -L`root-config --libdir` -L $(SPIDR_TPX3_LIB) -L $(STPX3DT_LIB) -L $(NI_LIBS) -L $(CONRAD_LIB)
LDLIBS   = `root-config --noldflags --glibs` -lSpidrTpx3Lib -lstpx3dt -lvisa -lnipalu -lbtsoftware_conrad


.PHONY: all clean cleanall

all: tpDelayScan calibrateDacs spidrPowerCycle monitorTriggerFrequency

DacCalibrationReader.o: DacCalibrationReader.cpp DacCalibrationReader.h

tpDelayScan: tpDelayScan.o DacCalibrationReader.o 
tpDelayScan.o: tpDelayScan.cpp DacCalibrationReader.h PulseDelayController.h ProgressPrinter.h

calibrateDacs: calibrateDacs.o 
calibrateDacs.o: calibrateDacs.cpp

spidrPowerCycle: spidrPowerCycle.o 
spidrPowerCycle.o: spidrPowerCycle.cpp

monitorTriggerFrequency: monitorTriggerFrequency.o 
monitorTriggerFrequency.o: monitorTriggerFrequency.cpp

clean:
	$(RM) DacCalibrationReader.o tpDelayScan.o calibrateDacs.o spidrPowerCycle.o monitorTriggerFrequency.o

cleanall: clean
	$(RM) tpDelayScan calibrateDacs spidrPowerCycle monitorTriggerFrequency
