#ifndef PROGRESS_PRINTER_H
#define PROGRESS_PRINTER_H

#include <iostream>
#include <iomanip>
#include <string>
#include <cstdint>
#include <sstream>
#include <cmath>
#include <chrono>
#include <thread>
#include <mutex>
#include <atomic>

namespace std { namespace chrono
{
	template<class Duration, class Rep, class Period>
	Duration floor(const duration<Rep,Period>& d)
	{
		namespace cn = std::chrono;
		typedef cn::duration<double, typename Duration::period> DoubleDuration;
		return Duration(int(std::floor(cn::duration_cast<DoubleDuration>(d).count())));
	}
	
	template<class Duration, class Rep, class Period>
	Duration ceil(const duration<Rep,Period>& d)
	{
		namespace cn = std::chrono;
		typedef cn::duration<double, typename Duration::period> DoubleDuration;
		return Duration(int(std::ceil(cn::duration_cast<DoubleDuration>(d).count())));
	}
}}

class ProgressPrinter
{
public:
	ProgressPrinter(const std::string& processingMsg="", const std::string& finishedMsg="", int64_t total=0, int decimals=1, std::ostream& os=std::cerr):
	os(&os),processingMsg(processingMsg),finishedMsg(finishedMsg),
	count(0),total(total),decimals(0),progressMultiplier(0),width(0),
	begin(std::chrono::steady_clock::now()),end(),currentProgress(0),currentTime(),currentFraction(0.),run(true),pauseOutput(false),thr(nullptr)
	{
		SetDecimals(decimals);
	}
	
	~ProgressPrinter()
	{
		if(thr)
		{
			run = false;
			thr->join();
			delete thr;
		}
	}
	
	void SetProcessingMessage(const std::string& processingMsg){ this->processingMsg = processingMsg; }
	void SetFinishedMessage  (const std::string&   finishedMsg){ this->  finishedMsg =   finishedMsg; }
	void SetCount(int64_t count){ this->count = count; }
	void SetTotal(int64_t total){ this->total = total; }
	void SetDecimals(int decimals)
	{ 
		this->decimals = decimals; 
		progressMultiplier = 100.*std::pow(10, decimals);
		width = 2 + (decimals? 1 : 0) + decimals;
	}
	
	void SetOutputStream(std::ostream& ostream){ this->os = &ostream; }
	
	std::chrono::steady_clock::time_point GetBegin() const { return begin; }
	std::chrono::steady_clock::time_point GetEnd  () const { return end  ; }
	int64_t GetCount() const { return count; }
	
	template<class time_point>
	void Start(int64_t count0, time_point begin0)
	{
		count = count0;
		run = true;
		pauseOutput = false;
		
		
		begin = std::chrono::time_point_cast<std::chrono::steady_clock::duration>(begin0);//!begin0? std::chrono::steady_clock::now() : std::chrono::steady_clock::time_point(std::chrono::steady_clock::duration(begin0));
		UpdateProgress(begin);
		
		if(!getenv("PBS_ENVIRONMENT") || std::string(getenv("PBS_ENVIRONMENT"))!="PBS_BATCH")
			thr = new std::thread(&ProgressPrinter::StatusPrinter, this); 
	}
	
	void Start(int64_t count0=0)
	{
		Start(count0, std::chrono::steady_clock::now());
	}
	void Stop ()
	{
		end = std::chrono::steady_clock::now();
		int64_t countRestore = count;
		count = total;
		std::ostringstream oss;
		UpdateProgress(end);
		
		if(thr)
		{  
			run = false;
			thr->join();
			delete thr;
			thr = nullptr;
			*os << "\r";
		}
		
		oss << finishedMsg << " " << FormatDuration(end - begin) << std::endl;
		*os << oss.str() << std::flush;
		
		count = countRestore;
	}
	
	void PauseOutput()
	{
		pauseOutput = true;
	}
	
	void ResumeOutput()
	{
		pauseOutput = false;
	}
	
	double Progress() const { return count*1./total; }
	
	int64_t Count() const { return count; }
	
	ProgressPrinter& operator --(){ auto t=std::chrono::steady_clock::now(); --count; UpdateProgress(t); return *this; }
	ProgressPrinter& operator ++(){ auto t=std::chrono::steady_clock::now(); ++count; UpdateProgress(t); return *this; }
	template<class T> ProgressPrinter& operator +=(const T& x){ auto t=std::chrono::steady_clock::now(); count+=x; UpdateProgress(t); return *this; }
	
protected:
	static constexpr int nDotsMax=3;
	std::ostream* os;
	std::string processingMsg;
	std::string finishedMsg;
	int64_t count;
	int64_t total;
	int decimals;
	double progressMultiplier;
	int width;
	std::chrono::steady_clock::time_point begin, end;
	// struct currentProgressType
	// {
	// 	int progress;
	// 	std::chrono::steady_clock::time_point time;
	// 	double fraction;
	// };
	std::mutex mtx;
	int currentProgress;
	std::chrono::steady_clock::time_point currentTime;
	double currentFraction;
	// std::atomic<currentProgressType> currentProgress;
	std::atomic<bool> run;
	std::atomic<bool> pauseOutput;
	std::thread* thr;
	
	template<class Rep, class Period>
	static std::string FormatDuration(const std::chrono::duration<Rep, Period>& duration)
	{
		namespace cn = std::chrono;
		typedef cn::duration<int, std::ratio<86400, 1>> cn__days;
		std::ostringstream oss;
		bool comma=false;
		
		auto dy = cn::floor<cn__days        >(duration                );
		auto hr = cn::floor<cn::hours       >(duration - (dy         ));
		auto mn = cn::floor<cn::minutes     >(duration - (dy+hr      ));
		auto sc = cn::floor<cn::seconds     >(duration - (dy+hr+mn   ));
		auto ms = cn::floor<cn::milliseconds>(duration - (dy+hr+mn+sc));
		
		oss << std::setfill('0');
		
		
		if(dy.count()) goto lbl_dy;
		if(hr.count()) goto lbl_hr;
		if(mn.count()) goto lbl_mn;
		goto lbl_sc;
		
		lbl_dy: oss << dy.count() << " day" << (dy.count()==1?"":"s") << ", ";
		lbl_hr: oss << hr.count() << " hour" << (hr.count()==1?"":"s") << ", "; comma=true;
		lbl_mn: oss << mn.count() << " minute" << (mn.count()==1?"":"s") << (comma?",":"") << " and ";
		lbl_sc: oss << sc.count() << '.' << std::setw(3) << ms.count() << " seconds";
		
		return oss.str();
	}
	
	template<class Rep, class Period>
	static std::string FormatRemaining(const std::chrono::duration<Rep, Period>& duration)
	{
		namespace cn = std::chrono;
		typedef cn::duration<int, std::ratio<86400, 1>> cn__days;
		std::ostringstream oss;
		int count=0;
		
		auto dy = cn::floor<cn__days        >(duration                );
		auto hr = cn::floor<cn::hours       >(duration - (dy         ));
		auto mn = cn::floor<cn::minutes     >(duration - (dy+hr      ));
		auto sc = cn::ceil<cn::seconds     >(duration - (dy+hr+mn   ));
		// auto ms = cn::floor<cn::milliseconds>(duration - (dy+hr+mn+sc));
		
		oss << std::setfill('0');
		
		
		if(dy.count()) goto lbl_dy;
		if(hr.count()) goto lbl_hr;
		if(mn.count()) goto lbl_mn;
		goto lbl_sc;
		
		lbl_dy: oss << dy.count() << " day" << (dy.count()==1?"":"s"); if(++count==1) oss << " and "; else goto lbl_en;
		lbl_hr: oss << hr.count() << " hour" << (hr.count()==1?"":"s"); if(++count==1) oss << " and "; else goto lbl_en;
		lbl_mn: oss << mn.count() << " minute" << (mn.count()==1?"":"s"); if(++count==1) oss << " and "; else goto lbl_en;
		lbl_sc: oss << sc.count() << " second" << (sc.count()==1?"":"s");
		
		lbl_en:		
		return oss.str();
	}
	
	
	void UpdateProgress(std::chrono::steady_clock::time_point t)
	{
		mtx.lock();
		currentProgress = int(floor(std::min(std::max(0l, count), total)*progressMultiplier/total));
		currentTime = t;
		currentFraction = count*1./total;
		mtx.unlock();
	}
	
	void StatusPrinter()
	{
		namespace cn = std::chrono;
		auto next  = begin;
		int duration=0, previousDuration=0, previousLength=0;
		int previousProgress = 0;
		std::string remaining = "";
		
		
		while(run)
		{
			mtx.lock();
			const auto progress = currentProgress;
			const auto fractDur = currentTime - begin;
			const auto fraction = currentFraction;
			mtx.unlock();
			// const auto [progress, t, f] = currentProgressType(currentProgress);
			
			if(previousProgress != progress || previousDuration != duration || next==begin)
			{
				int nDots = duration%(nDotsMax+1);
							
				if((previousDuration!=duration || remaining.empty()) && cn::floor<cn::seconds>(fractDur).count()>1 /*&& currentProgress>0*/ && fraction<1.)
					remaining = FormatRemaining(cn::duration<double, decltype(fractDur)::period>(fractDur.count()*(1./fraction - 1.))) + " remaining";
					
				previousProgress = progress;
				previousDuration = duration;
				
				std::ostringstream oss;
				oss << "\r" << processingMsg 
					<< std::setfill('.') << std::setw(nDots) << ""
					<< std::setfill(' ') << std::setw(nDotsMax-nDots) << ""
					<< " [" << std::setw(width) << std::fixed << std::setprecision(decimals) << progress*100./progressMultiplier << "%]";
				if(fraction>0.)
					oss << " " << remaining;
					
					
				int length = oss.str().length();
				
				if(length < previousLength)
					oss << std::setfill(' ') << std::setw(previousLength - length) << "";
				
				previousLength = length;
				
				if(!pauseOutput)
					*os << oss.str() << std::flush;
			}
			
			auto now = cn::steady_clock::now();
			while(next < (now = cn::steady_clock::now())) next += cn::milliseconds(100);
			duration = cn::duration_cast<cn::seconds>(next - begin).count();
			std::this_thread::sleep_for(next - now);
		}
		
		*os << "\r" << std::setfill(' ') << std::setw(previousLength+3) << "" << "\r" << std::flush;
	}
};


#endif //PROGRESS_PRINTER_H
