#include <iostream>
#include <iomanip>
#include <cmath>
#include <set>
#include <unordered_set>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm> 
#include <fstream> 
#include <sstream>
#include <numeric>
#include <iterator>
#include <map>
#include <unordered_map>
#include <chrono>
#include <ctime>
#include <limits>
#include <exception>
#include <csignal>

#include <sys/stat.h>

#include <thread>
#include <mutex>

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TGraphErrors.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "TH1D.h"
#include "TMacro.h"

#include "SpidrController.h"
#include "SpidrDaq.h"
#include "tpx3defs.h"
#include "tpx3dacsdescr.h"

#include "PulseDelayController.h"
#include "DacCalibrationReader.h"
#include "ProgressPrinter.h"

#include "stpx3dt/Packet.h"
#include "stpx3dt/PacketStream.h"
#include "stpx3dt/PixelSorter.h"
#include "stpx3dt/SortedPacketStream.h"

#include "ConradController.h"

using namespace std;


// Some constants
const double testPulseCapacitance = 3.235e-15; // [fF]
const double elementaryCharge     = 1.6021766208e-19;

// Some paths
const string configsDir  = "/localstore/TPX3/CONFIGS";
const string dataBaseDir = "/localstore/TPX3/DATA/tpDelayScan";
      string dataDir     = ""; // Set later. Depends on the usePixelMapFile flag

// SPIDR parameters
const int         spidrAddr[] = {192,168,1,10};
const int         spidrPort   = 50000;
const bool        extClk      = false;

const vector<int> devices     = {0}; // devices to measure
const int         links       = 0x0F; // Links to enable
const int         hdmiSwitch  = 0xFFFFF6;

// Measurement parameters
      double  threshold    = -1.; // Threshold [electrons]. The threshold setting of the DAC config file is used when this value is negative. (Signal polarity is automatically taken into account)
const int     polarity     = 1;
const int     tpDigital    = false;

const bool    partialScan  = true; // Enable pixels form the pixel file if true, otherwise use the pixel spacing
const string  pixelMapFile = "./pixelMaps/pixelMap1.txt";
const int     colSpacing   = 8; //
const int     rowSpacing   = 8; // Make sure these divide 256

const int     tpCount      = 1000;
      double  tpCharge     = 10000; // Target test pulse charge in electrons. 16000 e is about the maximum

const int     tpPeriod     = 1<<15; // Test pulse period in 25 ns clock cycles. Should be the prescaler setting.
const double  tpFrequency  = 40.e6/tpPeriod;

const int64_t tdcOffset    = - 8*96; // -200 ns
const int64_t tdcWindow    =  12*96; //  300 ns

const auto    delayStep    = chrono::duration<int, pico>(100);
const auto    delayRange   = chrono::duration<double, nano>(25);
const int     nDelaySteps  = int(ceil(delayRange/delayStep)); // Derived


// DAQ setting
const int64_t daqBufferSize = 0x20000000; // 512 MB.


// Timepix3 configuration
const int genCfg = (polarity<0? TPX3_POLARITY_EMIN : TPX3_POLARITY_HPLUS) | 
                   TPX3_ACQMODE_TOA_TOT  | 
                   TPX3_GRAYCOUNT_ENA    | 
                   TPX3_TESTPULSE_ENA    | 
                   TPX3_FASTLO_ENA       | 
                   (tpDigital?  TPX3_SELECTTP_DIGITAL : 0) |
                   TPX3_SELECTTP_EXT_INT ;

const int pllCfg = TPX3_PLL_RUN          |
                   TPX3_VCNTRL_PLL       |
                   TPX3_DUALEDGE_CLK     |
                   TPX3_PHASESHIFT_DIV_8 |
                   TPX3_PHASESHIFT_NR_16 ;
                   
const int nSpidrAttempts = 1;

// Test pulse generator parameters
const string pulGenAddr    = "TCPIP::192.168.1.4::INSTR";
const auto   tdcPulseWidth = chrono::microseconds(1);
const auto   digPulseWidth = chrono::microseconds(1); // Digital test pulse width 
const int     tpChannel    = 1;
const int    tdcChannel    = 2;

struct DeviceStruct
{	
	int index;
	string id;
	SpidrDaq* daq;
	unordered_set<uint16_t>* maskedPixels;
	
	TFile* file;
	TTree* tdcTree;
	int64_t tdcTimestamp;
	
	TTree* hitTree;
	uint8_t hitCol;
	uint8_t hitRow;
	int64_t hitToa;
	int32_t hitCount;
	
	int32_t delay;
	
	TH2D* hitmap;
	TH1D* totHis;
	TH1D* resHis;
	TH1D* cntHis;
	
	double charge;       // Measured using ADC and coverted to charge with tp capacitance
	double chargeErr;    // Uncertainty due to ADC
	double threshold;    // Measured using ADC and coverted to charge with fb capacitance (same as tp)
	double thresholdErr; // Uncertainty due to ADC
	
	array<int, TPX3_DAC_COUNT_TO_SET> dacValues;
	
	TMacro* macro;
	
	// Markers
	uint32_t beginTimeHigh;
	uint32_t beginTimeLow;
	uint32_t endTimeHigh;
	uint32_t endTimeLow;
};

bool Error(const std::string& errMsg);
string ErrStatString(int errStat);
string FormatDeviceId(int device_id_int);
string MakeFilenameTimestamp();


SpidrController* ctrl = nullptr; // Spidr connection
vector<DeviceStruct*> activeDevices; // To store the active device variables
string filenameTimestamp = MakeFilenameTimestamp();
// vector<string> incompleteFiles; // Filenames to resume a measurement
set<int> pixelMap;
map<int, int> dacOverrides;

void InitTpx3(); // Connect to the spidr and configure
void InitOutput(); // Initialises the output root files
void T0Sync();
void HeartBeat(int marker);
void BeginMarker(){ HeartBeat(0); }
void EndMarker(){ HeartBeat(1); }
void SetReadout(bool state);
void SetShutter(bool state);
void SetPixelMap(int colPhase, int rowPhase);
void SetPixelMap(const string& filename);
void ConfigurePixels();
void ProcessSamples(int delay);
void FlushSamples();
int GetSendPackets();
int GetReceivedPackets();


PulseDelayController* tpdc;

void InitPulseGen();


const string conradComport = "/dev/ttyUSB0";
const int    conradChannel = 1;

void SpidrPowerCycle();


// Signal handling
int signum = 0;

void signalHandler(int signum);
void SetSignalHandlers(bool set=true);


class critical_error : public exception
{
public:
	critical_error(const string& msg):exception(),message(msg){}
	
	const char* what() const throw(){ return message.c_str(); }
	
protected:
	string message;
};

template<class T, int N> T GetArgument(int argc, char** argv)
{
	stringstream ss;
	T value;
	
	if(N >= argc)
	{
		cerr << "Expecting more command line arguments" << endl;
		exit(1);
	}
	
	if(!(ss << argv[N] && ss >> value))
	{
		cerr << "Error parsing command line argument" << endl;
		exit(1);
	}
	
	return value;
}

template<class T> T FromString(const char* arg);

int main(int argc, char** argv)
{
	
	dataDir = dataBaseDir + (partialScan? "Partial" : "Full");
	
	if(argc >= 3)
	{
		dataDir  = dataBaseDir + "/" +GetArgument<string, 1>(argc, argv);
		tpCharge = GetArgument<int, 2>(argc, argv);
		
		mkdir(dataDir.c_str(), 0755);
	}
	else
	{
		cerr << "Expecting a test pulse charge as argument" << endl;
		return 0;
	}
	
	if(argc >= 4)
		threshold = GetArgument<int, 3>(argc, argv);
	
	if(argc >= 5 && (argc&1))
	{
		cerr << "Expecting an even number of arguments for the dac settings: [<dac_code> <dac_value>]..." << endl;
		return 0;
	}
		
	for(int argi=4; argi+1<argc; argi+=2)
	{
		const int dacCode  = FromString<int>(argv[argi  ]);
		const int dacValue = FromString<int>(argv[argi+1]);
		
		if(dacCode < 1 || dacCode > TPX3_DAC_COUNT_TO_SET)
		{
			cerr << "Unknown DAC code: dacCode" << endl;
			return 0;
		}
		
		if(dacValue < 0 || dacValue >= 1<<TPX3_DAC_TABLE[dacCode-1].bits)
		{
			cerr << "Value of " << dacValue << " for " << TPX3_DAC_TABLE[dacCode-1].name << " is out of range (0--" << ((1<<TPX3_DAC_TABLE[dacCode-1].bits)-1) << ")" << endl;
			return 0;
		}
		
		cerr << "Using " << TPX3_DAC_TABLE[dacCode-1].name << " = " << dacValue << endl;
		
		dacOverrides[dacCode] = dacValue;
	}
	
	
	// Initialise
	const int initAttempts=5;
	int initAttempt=0;
	
	while(true)
		try
		{
			if(initAttempt==initAttempts) 
			{
				cerr << "Error: Failed to initialise the pulse generator too many times" << endl;
				return 1;
			}
			++initAttempt;
				
			InitPulseGen();
			InitTpx3();
			
			// Load the pixel map file for a partial scan
			if(partialScan)
				SetPixelMap(pixelMapFile);
				
			InitOutput();
			break;
		}
		catch(std::exception& e)
		{
			cerr << e.what() << endl;
			SpidrPowerCycle();
			continue;
		}
		
	initAttempt = 0;
	
	tpdc->Start();
	
	SetSignalHandlers();
	
	// Do the measurements
	chrono::duration<double> measurementDuration(tpCount/tpFrequency);
	const int nMeasurements = partialScan? nDelaySteps : colSpacing*rowSpacing*nDelaySteps;
	ProgressPrinter progress("Measuring", "Completed in", nMeasurements);
	
	bool initTpx3 = false;
	bool initPulseGen = false;
	bool critical = false;
	int delay = 0;
	
	// if(partialScan)
	// 	SetPixelMap(pixelMapFile);
	
	progress.Start();
	
	for(int colPhase=0; colPhase<colSpacing && !signum; ++colPhase)
	for(int rowPhase=0; rowPhase<rowSpacing && !signum; ++rowPhase)
	try
	{
		if(initPulseGen)
		{
			cerr << "Reinitialising The pulse generator" << endl;
			if(initAttempt==initAttempts) throw critical_error("Error: Failed to initialise the pulse generator too many times");
			++initAttempt;
			InitPulseGen();
			initPulseGen = false;
			progress.ResumeOutput();
		}
		
		if(initTpx3)
		{
			cerr << "Reinitialising Timepix3" << endl;
			++initAttempt;
			SpidrPowerCycle();
			InitTpx3();
			initTpx3 = false;
			progress.ResumeOutput();
		}
		initAttempt=0;
		
		if(!partialScan)
			SetPixelMap(colPhase, rowPhase);
		
		ConfigurePixels();
		this_thread::sleep_for(chrono::milliseconds(100)); // Wait for a bit
		T0Sync(); // Pixel times are wrong after configuring the pixel matrix
		this_thread::sleep_for(chrono::milliseconds(100)); // Wait for a bit
		
		int packetsSendBefore     = GetSendPackets();
		int packetsReceivedBefore = GetReceivedPackets();
		
		SetReadout(true);
				
		// Do the delay scan
		for(; delay<nDelaySteps && !signum; ++delay, ++progress)
		{
			tpdc->SetDelay(delay*delayStep);
			if(!ctrl->resetCounters()) throw runtime_error("Error resetting counters: " + ctrl->errorString());
			
			this_thread::sleep_for(chrono::milliseconds(100)); // Give the pulse generator some time to do its thing
			
			BeginMarker();
						
			if(!ctrl->startAutoTrigger()) throw runtime_error("Error starting auto trigger: " + ctrl->errorString());
			this_thread::sleep_for(measurementDuration); 
			
			for(int attempts=0;;)
			{
				int nShutters = 0;
				if(!ctrl->getShutterCounter(&nShutters)) throw runtime_error("Error getting shutter counter: " + ctrl->errorString());
				if(nShutters) break;
				if(++attempts == 50) throw runtime_error("Timeout waiting for shutter");
				this_thread::sleep_for(chrono::milliseconds(100));
			}
			
			EndMarker();
			
			bool missingPackets = false;
			int received = 0;
			int send = 0;
			int packetsSendAfter = 0;
			int packetsReceivedAfter = 0;
			
			for(int attempts=0;;)
			{
				packetsSendAfter = GetSendPackets();
				packetsReceivedAfter = GetReceivedPackets();
				received = packetsReceivedAfter - packetsReceivedBefore;
				send = packetsSendAfter - packetsSendBefore;
				if(received >= send) break;
				if(++attempts == 500){ missingPackets = true; break; }
				this_thread::sleep_for(chrono::milliseconds(10));
			}
			
			packetsReceivedBefore = packetsReceivedAfter;
			packetsSendBefore     = packetsSendAfter;
		
			if(missingPackets)
			{
				progress.PauseOutput();
				this_thread::sleep_for(chrono::milliseconds(150)); 
				cerr << endl << "Error: Only received " << received << " out of " << send << " packets" << endl;
				cerr << "Repeating last measurement" << endl;
				--progress;
				progress.ResumeOutput();
				--delay;
				FlushSamples();
				continue;
			}
			
			ProcessSamples(delay);
		}
		
		this_thread::sleep_for(chrono::milliseconds(100)); 
		
		SetReadout(false);
		
		delay = 0;
		
		if(partialScan)
			goto nestedLoopBreak;
	}
	catch(PulseDelayController::Error& e)
	{
		progress.PauseOutput();
		this_thread::sleep_for(chrono::milliseconds(150)); 
		cerr << endl << e.what() << endl;
		initPulseGen = true;
		--rowPhase;
		this_thread::sleep_for(chrono::milliseconds(1000));
	}
	catch(runtime_error& e)
	{
		progress.PauseOutput();
		this_thread::sleep_for(chrono::milliseconds(150)); 
		cerr << endl << e.what() << endl;
		initTpx3 = true;
		--rowPhase;
		this_thread::sleep_for(chrono::milliseconds(1000));
	}
	catch(PulseDelayController::CriticalError& e)
	{
		progress.PauseOutput();
		this_thread::sleep_for(chrono::milliseconds(150)); 
		cerr << endl << e.what() << endl;
		critical = true;
		goto nestedLoopBreak;
	}
	catch(critical_error& e)
	{
		progress.PauseOutput();
		this_thread::sleep_for(chrono::milliseconds(150)); 
		cerr << endl << e.what() << endl;
		critical = true;
		goto nestedLoopBreak;
	}
	
nestedLoopBreak:
	
	if(signum || critical)
		progress.SetFinishedMessage("Cancelled after");
	
	progress.Stop();
	
	tpdc->Stop();
	
	
	// Store the output
	for(auto& dev : activeDevices)
	{
		dev->tdcTree->AutoSave();
		dev->hitTree->AutoSave();
		dev->file->Write(0, TObject::kWriteDelete);
		dev->file->Close();
	}
		
	// Clean up
	for(auto& dev : activeDevices)
	{
		delete dev->daq;
		delete dev->maskedPixels;
		delete dev->file;
	}
	
	delete ctrl;
	
	return critical || signum? 1 : 0;
}

bool Error(const std::string& errMsg)
{
	std::cerr << errMsg << std::endl;
	
	return false; 
}

string ErrStatString(int errStat)
{
	const char* resetErrorsByte0[] = {
		"",
		"TPX3_ERR_SC_ILLEGAL", 
		"TPX3_ERR_SC_STATE", 
		"TPX3_ERR_SC_ERRSTATE", 
		"TPX3_ERR_SC_WORDS", 
		"TPX3_ERR_TX_TIMEOUT", 
		"TPX3_ERR_EMPTY", 
		"TPX3_ERR_NOTEMPTY", 
		"TPX3_ERR_FULL", 
		"TPX3_ERR_UNEXP_REPLY", 
		"TPX3_ERR_UNEXP_HDR"
	};

	const char* resetErrorsByte1[] = {
		"",
		"SPIDR_ERR_I2C_INIT",
		"SPIDR_ERR_GTX_INIT",
		"SPIDR_ERR_MAX6642_INIT",
		"SPIDR_ERR_INA219_0_INIT",
		"SPIDR_ERR_INA219_1_INIT",
		"SPIDR_ERR_I2C"
	};

	const char* resetErrorsByte3[] = {
		"",
		"STORE_ERR_TPX",
		"STORE_ERR_WRITE",
		"STORE_ERR_WRITE_CHECK",
		"STORE_ERR_READ",
		"STORE_ERR_UNMATCHED_ID",
		"STORE_ERR_NOFLASH"
	};

	ostringstream oss;
	int b0 = errStat >>  0 & 0xFF;
	int b1 = errStat >>  8 & 0xFF;
	int b3 = errStat >> 24 & 0xFF;
	
	if(b3) oss << resetErrorsByte3[b3];
	if(b1) oss << (oss.str().empty()?"":", ") << resetErrorsByte1[b1];
	if(b0) oss << (oss.str().empty()?"":", ") << resetErrorsByte0[b0];
	
	return oss.str();
}

string FormatDeviceId(int device_id_int)
{
    int wafer_number = device_id_int >> 8 & 0xFFF;
    int y_position   = device_id_int >> 4 & 0xF;
    int x_position   = device_id_int >> 0 & 0xF;
	ostringstream oss;
	
	oss << setfill('0');
	oss << 'W' << setw(4) << wafer_number << '_' << char('A' + x_position - 1) << setw(2) << y_position;
	
    return oss.str();
}

void InitTpx3()
{
	if(activeDevices.empty())
		for(const int devIndex : devices)
		{
			auto dev = new DeviceStruct;
			
			dev->index        = devIndex;
			dev->file         = nullptr;
			dev->daq          = nullptr;
			dev->maskedPixels = nullptr;
			
			activeDevices.push_back(dev);
		}
	else
		for(auto& dev : activeDevices)
		{
			if(dev->daq)          delete dev->daq;
			if(dev->maskedPixels) delete dev->maskedPixels;
			
			dev->daq          = nullptr;
			dev->maskedPixels = nullptr;
		}

	if(ctrl)
	{
		delete ctrl;
		
		// This seems to do the trick for reviving CERN boards...
		// system("spidrreset 192.168.1.10");
		// system("Tpx3daq -m -i 1 -t 1 -p -1 -s CernBoardHack");
		// system("rm /localstore/TPX3/DATA/CHIP0/Test/CernBoardHack*.dat");
	}
	
	// Setup the SPIDR connection
	int errStat  = 0;
	
	
	ctrl = new SpidrController(spidrAddr[0], spidrAddr[1], spidrAddr[2], spidrAddr[3], spidrPort);
	
	if(!ctrl->isConnected()) throw runtime_error("Cannot connect to SPIDR: " +  ctrl->connectionErrString() + " (" +  ctrl->ipAddressString() + ", " + ctrl->connectionStateString() + ")");
	
	cerr << "Connected to SPIDR at " << ctrl->ipAddressString() << endl;
	
	// ctrl->setChipboardId(0x02000000);
	// ctrl->setChipboardId(0x02000051);
	// ctrl->setChipboardId(-1);
	
	// Check board ID
	int boardId=0;
	
	
	if(!ctrl->getChipboardId(&boardId)) throw runtime_error("Error getting board ID: " + ctrl->errorString());
	
	cerr << hex << showbase;
	
	// if(boardId == -1 || boardId == 0)
	// {
	// 	const int oldId = boardId;
	// 	boardId = 0x01000000;
	// 	// boardId = 0x02000000;
	// 	cerr << "Unknown board ID (" << oldId << "). Setting to " << boardId << endl;
	// 	if(!ctrl->setChipboardId(boardId)) throw runtime_error("Error setting board ID: " + ctrl->errorString());
	// }
	
	cerr << "Board ID: " << boardId << dec << endl;
	
		
	// Some initial configuration first
	int softVer  = 0;
	int firmVer  = 0;
	int nDevices = 0;
	
	if(!ctrl->setExtRefClk(extClk)) throw runtime_error("Error setting external clock: " + ctrl->errorString());
	
	if(!ctrl->reset(&errStat)) throw runtime_error("Error resetting SPIDR: " + ctrl->errorString());
	this_thread::sleep_for(chrono::milliseconds(100));
	
	
	if(!ctrl->resetDevice(0)) throw runtime_error("Error resetting device: " + ctrl->errorString());
	
	this_thread::sleep_for(chrono::milliseconds(100));
	
	
	if(!ctrl->reinitDevices()) throw runtime_error("Error reinitialising devices: " + ctrl->errorString());	
	
	this_thread::sleep_for(chrono::milliseconds(100));
	

	
	
	if(!ctrl->getSoftwVersion(&softVer)) throw runtime_error("Error getting software version: " + ctrl->errorString());
	if(!ctrl->getFirmwVersion(&firmVer)) throw runtime_error("Error getting firmware version: " + ctrl->errorString());
	if(!ctrl->getDeviceCount(&nDevices)) throw runtime_error("Error getting number of supported devices: " + ctrl->errorString());
	
	cerr << "Using " << (extClk?"ex":"in") << "ternal clock" << endl;
	
	cerr << "Reset error status: " << hex << errStat << dec << " (" << ErrStatString(errStat) << ")" << endl;
		
	cerr << hex << showbase
	     << "Class version:    " << ctrl->classVersion() << endl
	     << "Software version: " << softVer              << endl
	     << "Firmware version: " << firmVer              << endl
	     << dec << noshowbase
	     << "Supported devices: " << nDevices             << endl;
	
	this_thread::sleep_for(chrono::milliseconds(1));
	
	
	if(!ctrl->setSpidrReg(0x2B8, 0)) throw runtime_error("Error enabling TDC: " + ctrl->errorString()); // Enable TDC
	if(!ctrl->setSpidrReg(0x810, hdmiSwitch)) throw runtime_error("Error setting HMDI: " + ctrl->errorString()); // Configure HDMI
	if(!ctrl->setDecodersEna(true)) throw runtime_error("Error enabling decoder: " + ctrl->errorString()); // Let the fpga do the LFSR/gray decoding
	
	
	// Check if the devies are active and make daq objects
	cerr << "----------------------------------" << endl
		 << " Device           Links            " << endl
		 << " Num  Chip ID     Enabled  Locked " << endl;
	
	for(auto& dev : activeDevices)
	{
		// DeviceStruct* dev = nullptr;
		// int status = 0;
		int enabledMask = 0;
		int lockedMask  = 0;
		int devId;
		
	
		if(!ctrl->setOutputMask(dev->index, links)) throw runtime_error("Error setting Timepix3 output links: " + ctrl->errorString());
		if(!ctrl->setSpidrReg(0x300 + (dev->index<<2), (~links)&0xFF)) throw runtime_error("Error setting SPIDR3 links: " + ctrl->errorString());
		if(!ctrl->getLinkStatus(dev->index, &enabledMask, &lockedMask)) throw runtime_error("Error getting link status: " + ctrl->errorString());
		if(!ctrl->getDeviceId(dev->index, &devId)) throw runtime_error("Error getting device id: " + ctrl->errorString());
		
		dev->id = FormatDeviceId(devId);
		
		dev->daq = new SpidrDaq(ctrl, daqBufferSize, dev->index);
		
		if(dev->daq->hasError())
			throw runtime_error("Error creating SpidrDaq object: " + dev->daq->errorString());
		
		// Set the daq up for sampling all data
		dev->daq->setFlush(false);
		dev->daq->setSampling(true);
		dev->daq->setSampleAll(true);
		
		
		// Print some device info
		ostringstream oss;
		
		oss << setw( 4) << dev->index
		    << setw(11) << dev->id
		    << hex
		    << setw( 8) << "0x" << setfill('0') << setw(2) << enabledMask << setfill(' ')
		    << setw( 6) << "0x" << setfill('0') << setw(2) << lockedMask << setfill(' ');
		
		cerr << oss.str() << endl;
		
		if(!(enabledMask &  lockedMask)) throw runtime_error("Device "+to_string(dev->index)+" is not active.");
		if(  enabledMask != lockedMask ) throw runtime_error("Device "+to_string(dev->index)+" has missing links.");
	}
	
	cerr << "----------------------------------" << endl;
	
	
	// Configure the devices
	for(auto& dev : activeDevices)
	{
		dev->maskedPixels = new unordered_set<uint16_t>;
		
		// Configure the DACS
		const string dacFilename = configsDir + "/" + dev->id + "_dacs.txt";
		ifstream dacIfs(dacFilename);
		int vthrCoarseBaseline = 0; // Need to remeber this value for tuning the fine threshold value below
		int vthrFineBaseline = 0; // Fine threshold level that corresponds to the baseline. Often included in the DAC file as a comment after the fine threshold setting
		int vthrFine = 0;
		
		if(!ctrl->setDacsDflt(dev->index)) throw runtime_error("Error setting default DAC values for device " + to_string(dev->index) + ": " + ctrl->errorString()); // First set the default values
		
		if(!dacIfs) // Cannot open the dac file, use default values
			cerr << "Cannot open DAC configuration file " << dacFilename << ". Using default values. " << endl;
		else
		{
			string line;
			
			cerr << "Setting DAC values of " << dev->id << " according to " << dacFilename << endl;
			
			while(dacIfs && getline(dacIfs, line))
			{
				int code, value;
				
				if(line.empty() || line.front() == '#') continue; // Skip lines beginning with #
				
				if(!(istringstream(line) >> code >> value)) throw critical_error("Error parsing DAC configuration file.");
				if(dacOverrides.count(code)) value = dacOverrides.at(code);
				if(!ctrl->setDac(dev->index, code, value)) throw runtime_error("Error setting DAC "+string(TPX3_DAC_TABLE[code-1].name)+": " + ctrl->errorString());
				dev->dacValues[code-1] = value;
				if(code == TPX3_VTHRESH_COARSE) vthrCoarseBaseline = value;
				if(code != TPX3_VTHRESH_FINE) continue;
				
				vthrFine = value;
				
				auto p = line.find_last_of('#');
				
				if(p == string::npos || !(istringstream(line.substr(p+1)) >> vthrFineBaseline))
					throw critical_error("Cannot find fine threshold baseline in DAC configuration file.");
			}
			
			dacIfs.close();
		}
		
		
		// Configure the pixels
		const string pixFilename = configsDir + "/" + dev->id + "_trimdacs.txt";
		ifstream pixIfs(pixFilename);
		
		if(!ctrl->resetPixels(dev->index)) throw runtime_error("Error resetting pixel configuration of device " + to_string(dev->index) + ": " + ctrl->errorString()); // First reset the pixel configuration on the chip.
		if(!ctrl->selectPixelConfig(dev->index)==dev->index) throw runtime_error("Error selecting pixel configuration " + to_string(dev->index) + ": " + ctrl->errorString());// Select the locally stored pixel configuration
		ctrl->resetPixelConfig(); // Clear the locally stored pixel configuration. (Cannot fail.)
		
		if(!pixIfs) // Cannot open pixel configuration file, use default values
			cerr << "Cannot open pixel configuration file " << pixFilename << ". Using default values. " << endl;
		else
		{
			string line;
			
			cerr << "Setting pixel configuration of " << dev->id << " according to " << pixFilename << endl;
			
			while(pixIfs && getline(pixIfs, line))
			{
				int col, row, threshold, mask;
				bool tp_ena; 
				
				if(line.empty() || line.front() == '#') continue; // Skip lines beginning with #
				if(!(istringstream(line) >> col >> row >> threshold >> mask >> tp_ena)) throw runtime_error("Error parsing pixel configuration file: " + line);
				
				if(!ctrl->setPixelThreshold(col, row, threshold)) throw runtime_error("Error setting pixel threshold: " + ctrl->errorString()); // Set the threshold tuning
				if(!ctrl->setPixelMask(col, row, mask)) throw runtime_error("Error setting pixel mask bit: " + ctrl->errorString()); // Set the pixel mask
				if(!ctrl->setPixelTestEna(col, row, false)) throw runtime_error("Error setting test pulse enable bit: " + ctrl->errorString()); // Set the pixel mask   // Set the test pulse bit
				
				if(mask)
					dev->maskedPixels->insert(col << 8 | row);
			}
			
			pixIfs.close();
			
			if(!ctrl->setPixelConfig(dev->index)) throw runtime_error("Error loading pixel configuration to device " + to_string(dev->index) + ": " + ctrl->errorString());
			
			// TODO check pixel configuration
		}
		
		
		// Set the configuration and masks
		int ethMask = 0;
		int cpuMask = 0;
		
		if(!ctrl->setGenConfig(dev->index, genCfg)) throw runtime_error("Error setting general configuration of device " + to_string(dev->index) + ": " + ctrl->errorString()); // Set the general configuration
		if(!ctrl->setPllConfig(dev->index, pllCfg)) throw runtime_error("Error setting PLL configuration of device " + to_string(dev->index) + ": " + ctrl->errorString()); // Set the PLL configuration
		if(!ctrl->getHeaderFilter(dev->index, &ethMask, &cpuMask)) throw runtime_error("Error getting header filters of device " + to_string(dev->index) + ": " + ctrl->errorString()); // First read the header filter masks
		if(!ctrl->setHeaderFilter(dev->index,   0xFFFF,  cpuMask)) throw runtime_error("Error setting header filters of device " + to_string(dev->index) + ": " + ctrl->errorString()); // Tell SPIDR to send all packets over ethernet.
		
		
		// Get the DAC calibration
		DacCalibrationReader dacCal;
		
		if(!dacCal.LoadDacCalibration("dacCalibrations/dacCalibration-"+dev->id+".root"))
			throw critical_error("Cannot find DAC calibration.");
		
		
		// Tune the threshold
		if(threshold >= 0.)
		{
			const double vthrb = dacCal.ThresholdVoltage(vthrCoarseBaseline, vthrFineBaseline);
			const int dacValStep = polarity<0?1:-1;
			double thrMismatch = 9999999.;
			int vthrCoarse = vthrCoarseBaseline;
			int vthrFine   = vthrFineBaseline;
			int fDacVal0   = vthrFineBaseline;
			
			for(int cDacVal=vthrCoarseBaseline; cDacVal>=0 && cDacVal<16; cDacVal+=dacValStep)
			{
				int localBestFine = 0;
				double localMismatch = 9999999.;
				
				for(int fDacVal=fDacVal0; fDacVal>=0 && fDacVal<512; fDacVal+=dacValStep)
				{
					const double vthr = dacCal.ThresholdVoltage(cDacVal, fDacVal);
					const double thr  = -polarity*(vthr - vthrb)*testPulseCapacitance/elementaryCharge;
					const double mis  = fabs(thr - threshold);
					
					if(thr >= 0. && mis < thrMismatch)
					{
						thrMismatch = fabs(thr - threshold);
						vthrCoarse   = cDacVal;
						vthrFine     = fDacVal;
					}
					
					if(thr >= 0. && mis < localMismatch)
					{
						localMismatch = mis;
						localBestFine = fDacVal;
					}
				}
				
				// If we are not at the limits of the fine setting we are done
				if(localBestFine != 0 && localBestFine != 511)
					break;
				
				fDacVal0 = dacValStep>0?0:511; // Set a new starting value for the fine dac value
			}
			
			
			const double vthr     = -polarity*(dacCal.ThresholdVoltage(vthrCoarse, vthrFine) - vthrb);
			const double vthrbErr = dacCal.ThresholdVoltageError(vthrCoarseBaseline, vthrFineBaseline);
			const double vthrErr  = sqrt(dacCal.ThresholdVoltageError(vthrCoarse, vthrFine)*dacCal.ThresholdVoltageError(vthrCoarse, vthrFine) + vthrbErr*vthrbErr);
			
			dev->threshold    = vthr   *testPulseCapacitance/elementaryCharge;
			dev->thresholdErr = vthrErr*testPulseCapacitance/elementaryCharge;
			
			dev->dacValues[TPX3_VTHRESH_COARSE-1] = vthrCoarse;
			dev->dacValues[TPX3_VTHRESH_FINE  -1] = vthrFine;
			
			cerr << "Coarse/fine threshold of " << dev->id << ": " << vthrCoarse << "/" << vthrFine << " (" << dev->threshold << " +/- " << dev->thresholdErr << " e)" << endl;
			
			if(!ctrl->setDac(dev->index, TPX3_VTHRESH_COARSE, vthrCoarse)) throw runtime_error("Error setting TPX3_VTHRESH_COARSE for device " + to_string(dev->index) + ": " + ctrl->errorString());
			if(!ctrl->setDac(dev->index, TPX3_VTHRESH_FINE  , vthrFine  )) throw runtime_error("Error setting TPX3_VTHRESH_FINE for device " + to_string(dev->index) + ": " + ctrl->errorString());
		}
		else
		{
			const double vthrb    = dacCal.ThresholdVoltage(vthrCoarseBaseline, vthrFineBaseline);
			const double vthr     = -polarity*(dacCal.ThresholdVoltage(vthrCoarseBaseline, vthrFine) - vthrb);
			const double vthrbErr = dacCal.ThresholdVoltageError(vthrCoarseBaseline, vthrFineBaseline);
			const double vthrErr  = sqrt(dacCal.ThresholdVoltageError(vthrCoarseBaseline, vthrFine)*dacCal.ThresholdVoltageError(vthrCoarseBaseline, vthrFine) + vthrbErr*vthrbErr);
			
			dev->threshold    = vthr   *testPulseCapacitance/elementaryCharge;
			dev->thresholdErr = vthrErr*testPulseCapacitance/elementaryCharge;
			
			cerr << "Coarse/fine threshold of " << dev->id << ": " << vthrCoarseBaseline << "/" << vthrFine << " (" << dev->threshold << " +/- " << dev->thresholdErr << " e)" << endl;
		}
		
		// Tune the test pusle voltage and configure the test pulse settings
		const double tpVoltage = tpCharge*elementaryCharge/testPulseCapacitance; // Target test pulse voltage (derived value)
		const int vtpCoarse = 64;
		int vtpFine = 0;
		double vtpMismatch = 9999999.;
		
		for(int dacVal=0; dacVal<512; ++ dacVal)
		{
			const double vtpc = dacCal.DacVoltage(TPX3_VTP_COARSE, vtpCoarse);
			const double vtpf = dacCal.DacVoltage(TPX3_VTP_FINE  , dacVal   );
			
			if(vtpf < vtpc) continue; // Otherwise the pulse should be inverted again
			
			
			double vtpm = fabs(fabs(vtpf - vtpc) - tpVoltage);
						
			if(vtpm >= vtpMismatch) continue;
			vtpMismatch = vtpm;
			vtpFine = dacVal;
		}
		
		const double vtpc    = dacCal.DacVoltage(TPX3_VTP_COARSE, vtpCoarse);
		const double vtpcErr = dacCal.DacVoltageError(TPX3_VTP_COARSE, vtpCoarse);
		const double vtpf    = dacCal.DacVoltage(TPX3_VTP_FINE  , vtpFine  );
		const double vtpfErr = dacCal.DacVoltageError(TPX3_VTP_FINE  , vtpFine  );
		const double tpVoltageReal    = fabs(vtpf - vtpc);
		const double tpVoltageRealErr = sqrt(vtpcErr*vtpcErr + vtpfErr*vtpfErr);
		
		dev->charge = tpVoltageReal*testPulseCapacitance/elementaryCharge;
		dev->chargeErr = tpVoltageRealErr*testPulseCapacitance/elementaryCharge;
		
		cerr << "Coarse/fine test pulse of " << dev->id << ": " << vtpCoarse << "(" << dacCal.DacVoltage(TPX3_VTP_COARSE, vtpCoarse) << ")/" << vtpFine << "(" << dacCal.DacVoltage(TPX3_VTP_FINE, vtpFine) << ") (" << tpVoltageReal << " +/- " << tpVoltageRealErr << "V, " << dev->charge << " +/- " << dev->chargeErr << " e)" << endl;
		
		dev->dacValues[TPX3_VTP_COARSE-1] = vtpCoarse;
		dev->dacValues[TPX3_VTP_FINE  -1] = vtpFine;
		
		// Set the internal test pulses
		if(!ctrl->setTpNumber(dev->index, 10)) throw runtime_error("Error setting Number of test pulses for device " + to_string(dev->index) + ": " + ctrl->errorString());
		if(!ctrl->setTpPeriodPhase(dev->index, 255, 0)) throw runtime_error("Error setting test pulses for device " + to_string(dev->index) + ": " + ctrl->errorString());
		if(!ctrl->setDac(dev->index, TPX3_VTP_COARSE, vtpCoarse)) throw runtime_error("Error setting TPX3_VTP_COARSE for device " + to_string(dev->index) + ": " + ctrl->errorString());
		if(!ctrl->setDac(dev->index, TPX3_VTP_FINE  , vtpFine  )) throw runtime_error("Error setting TPX3_VTP_FINE for device " + to_string(dev->index) + ": " + ctrl->errorString());
	}
	
	cerr << "---------------------------" << endl
	     << " Chip ID     Masked pixels " << endl;
	
	for(const auto& dev : activeDevices)
		cerr << setw(10) << dev->id << setw(16) << dev->maskedPixels->size() << endl;
	
	cerr << "---------------------------" << endl;
	
	if(!ctrl->resetCounters()) throw runtime_error("Error resetting counters.");
		
	
	// Set the auto trigger
	int shutterLength = 25*int(tpCount/tpFrequency/25.e-9 + 0.5);
	if(!ctrl->setShutterTriggerCfg(SHUTTERMODE_AUTO, 0, shutterLength)) throw runtime_error("Error setting shutter trigger: " + ctrl->errorString()); // Set the shutter trigger
	
	T0Sync();
}

void InitOutput()
{
	// if(incompleteFiles.empty())
		for(auto& dev : activeDevices)
		{
			const string filename = static_cast<ostringstream&&>(ostringstream() << dataDir << "/" << dev->id << "-" << filenameTimestamp << ".root").str();
			// const string filename = "/dev/null";
			// const string filename = "./test.root";
			// const string filename = static_cast<ostringstream&&>(ostringstream() << dataDir << "/" << "test.root").str();
			
			cerr << "Writing device " << dev->id << " data to " << filename << endl;
			
			dev->file = new TFile(filename.c_str(), "RECREATE");
			
			if(!dev->file->IsOpen()) throw runtime_error("Cannot open "+filename+" for writing");
			
			const int maxHitCount = tpCount*1.25;
			
			// Plots
			dev->hitmap = new TH2D("hitmap", "Hitmap;Column number;Row number", 260, -2.5, 257.5, 260, -2.5, 257.5);
			dev->totHis = new TH1D("tothis", "Time over threshold histogram; Time over threshold [#times 25 ns]", 251, -0.5, 250.5);
			dev->resHis = new TH1D("reshis", "Hit time residuals;Time residuals [ns];Count", 640, -500., 500.);
			dev->cntHis = new TH1D("cnthis", "Number of hits per pixel per delay;Number of hits;Count", maxHitCount+1, -0.5, maxHitCount+0.5);
			
			// Trees
			dev->tdcTree = new TTree("tdc"        , "");
			dev->hitTree = new TTree("tpDelayScan", "");
			
			// Branches
			dev->tdcTree->Branch("timestamp", &dev->tdcTimestamp, "timestamp/L");
			dev->tdcTree->Branch("delay"    , &dev->delay       , "delay/I"    );
			
			dev->hitTree->Branch("col"  , &dev->hitCol, "col/b"  );
			dev->hitTree->Branch("row"  , &dev->hitRow, "row/b"  ); 
			dev->hitTree->Branch("toa"  , &dev->hitToa, "toa/L"  );
			dev->hitTree->Branch("delay", &dev->delay , "delay/I");
			dev->hitTree->Branch("count", &dev->hitCount , "count/I");
			
			// Store the parameters
			dev->macro = new TMacro("parameters");
			stringstream ss;
			string line;
			
			ss << fixed << setprecision(3) << left;
			
			ss << "polarity          " << (polarity>0?"+1":"-1") << endl
			   << "digital           " << (tpDigital?"true":"false") << endl
			   << "colSpacing        " << colSpacing << endl
			   << "rowSpacing        " << rowSpacing << endl
			   << "tpCount           " << tpCount << endl
			   << "tpCharge          " << dev->charge << endl
			   << "tpChargeErr       " << dev->chargeErr << endl
			   << "tpFrequency       " << tpFrequency << endl
			   << "tpDelayStep       " << delayStep.count() << endl
			   << "tpDelaySteps      " << nDelaySteps << endl
			   << "threshold         " << dev->threshold << endl
			   << "thresholdErr      " << dev->thresholdErr << endl;
			
			for(int dac=0; dac<TPX3_DAC_COUNT_TO_SET; ++dac)
				ss << setw(17) << TPX3_DAC_TABLE[dac].name << " " << dev->dacValues[dac] << endl;
			
			
			while(getline(ss, line))
				dev->macro->AddLine(line.c_str());
			
			dev->macro->Write();
		}
	// else
	// {
	// 	auto it = incompleteFiles.begin(); 
		
	// 	for(auto& dev : activeDevices)
	// 	{
	// 		dev->file = new TFile(it++->c_str(), "UPDATE");
			
	// 		if(!dev->file->IsOpen()) throw runtime_error("Cannot open "+string(dev->file->GetName())+" for writing");
			
	// 		// Plots
	// 		dev->hitmap = dev->file->Get<TH2D>("hitmap");
	// 		dev->totHis = dev->file->Get<TH1D>("tothis");
	// 		dev->resHis = dev->file->Get<TH1D>("reshis");
	// 		dev->cntHis = dev->file->Get<TH1D>("cnthis");
			
	// 		// Trees
	// 		dev->tdcTree = dev->file->Get<TTree>("tdc");
	// 		dev->hitTree = dev->file->Get<TTree>("tpDelayScan");
			
	// 		dev->tdcTree->SetBranchAddress("timestamp", (Long64_t*)&dev->tdcTimestamp);
	// 		dev->tdcTree->SetBranchAddress("delay"    , &dev->delay       );
			
	// 		dev->hitTree->SetBranchAddress("col"  , &dev->hitCol);
	// 		dev->hitTree->SetBranchAddress("row"  , &dev->hitRow); 
	// 		dev->hitTree->SetBranchAddress("toa"  , (Long64_t*)&dev->hitToa);
	// 		dev->hitTree->SetBranchAddress("delay", &dev->delay);
	// 		dev->hitTree->SetBranchAddress("count", &dev->hitCount);
	// 	}
	// }
}

void T0Sync()
{
	// TODO implement retry
	if(!ctrl->setSpidrRegBit(0x290, 5, true )) throw runtime_error("Error setting t0 bit: " + ctrl->errorString());
	if(!ctrl->setSpidrRegBit(0x290, 5, false)) throw runtime_error("Error resetting t0 bit: " + ctrl->errorString());
}

void HeartBeat(int marker)
{
	for(const auto& dev : activeDevices)
	{
		int attempts = 0;
		
		for(; attempts<nSpidrAttempts ; ++attempts)
		{
			if(attempts) this_thread::sleep_for(chrono::milliseconds(100));
			auto timeLow  = marker? &dev->endTimeLow  : &dev->beginTimeLow ;
			auto timeHigh = marker? &dev->endTimeHigh : &dev->beginTimeHigh;
			if(ctrl->getTimer(dev->index, timeLow, timeHigh)) break;
		}
		
		if(attempts == nSpidrAttempts)
			throw runtime_error("Error requesting time from device " + dev->id + ": " + ctrl->errorString());
	}
}

void SetReadout(bool state)
{
	int attempts = 0;
	
	for(; attempts<nSpidrAttempts ; ++attempts)
	{
		if(attempts) this_thread::sleep_for(chrono::milliseconds(100));
		if(state? ctrl->datadrivenReadout() : ctrl->pauseReadout()) break;
	}
	
	if(attempts == nSpidrAttempts)
		throw runtime_error("Error " + string(state?"starting":"pausing") + " data driven readout: " + ctrl->errorString());
}

void SetShutter(bool state)
{
	int attempts = 0;
	
	for(; attempts<nSpidrAttempts ; ++attempts)
	{
		if(attempts) this_thread::sleep_for(chrono::milliseconds(100));
		if(state? ctrl->openShutter() : ctrl->closeShutter()) break;
	}
	
	if(attempts == nSpidrAttempts)
		throw runtime_error("Error " + string(state?"opening":"closing") + " shutter: " + ctrl->errorString());
}


void SetPixelMap(int colPhase, int rowPhase)
{
	pixelMap.clear();
	
	for(int col=colPhase; col<256; col+=colSpacing)
	for(int row=rowPhase; row<256; row+=rowSpacing)
		pixelMap.insert(col << 8 | row);
}
void SetPixelMap(const string& filename)
{
	ifstream ifs(filename);
	
	if(!ifs)
		throw critical_error("Cannot open pixel map file " + filename);
	
	cerr << "Enabling pixels according to " << filename << endl;
	
	
	string line;
	
	while(ifs && getline(ifs, line))
	{
		int col, row;
		
		if(line.empty() || line.front() == '#') continue; // Skip lines beginning with #
		if(!(istringstream(line) >> col >> row)) throw critical_error("Error parsing pixel map file.");
		pixelMap.insert(col << 8 | row);
	}
}

void ConfigurePixels()
{
	for(const auto& dev : activeDevices)
	{
		int attempts = 0;
		
		for(; attempts<nSpidrAttempts; ++attempts)
		{
			
			if(!ctrl->resetPixels(dev->index))
				continue;
				
			ctrl->selectPixelConfig(dev->index);
			ctrl->setPixelTestEna(ALL_PIXELS, ALL_PIXELS, false);
			ctrl->setPixelMask(ALL_PIXELS, ALL_PIXELS, true);
			
			for(int col=0; col<256; ++col)
				ctrl->setCtprBit(col, 0);
							
			// for(int col=colPhase; col<256; col+=colSpacing)
			// for(int row=rowPhase; row<256; row+=rowSpacing)
			for(const int pixel : pixelMap)
			{
				const int col = pixel >> 8 & 0xFF;
				const int row = pixel >> 0 & 0xFF;
				
				if(dev->maskedPixels->count(pixel))
					continue;
				
				ctrl->setCtprBit(col, 1);
				ctrl->setPixelTestEna(col, row, true);
				ctrl->setPixelMask(col, row, false);
			}
			
			if(!ctrl->setCtpr(dev->index) || !ctrl->setPixelConfig(dev->index))
				continue;
			
			break;
		}
		
		if(attempts == nSpidrAttempts)
			throw runtime_error("Error configuring pixels: " + ctrl->errorString());
	}
}

void ProcessSamples(int delay)
{
	constexpr uint64_t linkProblemPacket = 0x780278027802aaaaull;
	
	for(auto& dev : activeDevices)
	{
		// Read the sampled data
		vector<int64_t> tdcs;
		vector<int64_t> hbts;
		// const int nPixels = 256*256/colSpacing/rowSpacing;
		map<int, vector<tuple<int64_t, int16_t, int64_t>>> hitsPerPixel;
		stpx3dt::Packet packet;
		
		if(dev->daq->bufferFullOccurred())
			throw runtime_error("Warning: Buffer overflow occurred");
		
		if(dev->daq->packetsLostCount())
			throw runtime_error("Warning: Packets lost due to buffer overflow");
		
		
		bool timeout         = false;
		bool linkProblem     = false;
		bool beginMarker     = false;
		bool endMarker       = false;
		// bool bufferExhausted = false;
		
		// thread sampleRetriever([&](){
		uint64_t packetValue = 0;
		unsigned maskedPixelCount = 0;
		unsigned pixelCount = 0;
		unsigned timeCount = 0;
		unsigned triggerCount = 0;
		unsigned beforeCount = 0;
		unsigned otherCount = 0;
		unsigned totalCount = 0;
		unsigned afterCount = 0;
		array<unsigned, 1<<8> beforeHeaderCounts = {0};
		array<unsigned, 1<<8> otherHeaderCounts  = {0};
		array<unsigned, 1<<8> afterHeaderCounts  = {0};
		
		const auto timeLimit = chrono::steady_clock::now() + chrono::milliseconds(5000);
		
		
		while(!linkProblem && !dev->daq->bufferEmpty() && dev->daq->getSample(0x1000000, 250))
		{
			if(chrono::steady_clock::now() > timeLimit)
			{
				timeout = true;
				break;
			}
			
			
			while((packetValue = dev->daq->nextPacket()))
			{
				if(packetValue == linkProblemPacket)
				{
					linkProblem = true;
					break;
				}
				
				packet.SetValue(packetValue);
				++totalCount;
				
				if(!beginMarker)
				{
					if(packet.IsRequestTime() && dev->beginTimeLow == packet.Low() && dev->beginTimeHigh == packet.High())
					{
						hbts.push_back(packet.Timestamp() >> 12);
						beginMarker = true;
						++timeCount;
					}
					else
					{
						++beforeCount;
						++beforeHeaderCounts[packet.Header()];
					}
				}
				else if(endMarker)
				{
					++afterCount;
					++afterHeaderCounts[packet.Header()];
				}
				else if(packet.IsPixelData())
				{
					const int col = packet.Col();
					const int row = packet.Row();
					const int pix = col<<8 | row;
					
					// if(col % colSpacing != colPhase || row % rowSpacing != rowPhase)
					if(!pixelMap.count(pix))
					{
						++maskedPixelCount;
						continue;
					}
					
					// hitsPerPixel[(col/colSpacing)*256/rowSpacing + row/rowSpacing].emplace_back(packet.Timestamp() >> 8, packet.ToT(), 0);
					hitsPerPixel[pix].emplace_back(packet.Timestamp() >> 8, packet.ToT(), 0);
					++pixelCount;
				}
				else if(packet.IsTrigger())
				{
					tdcs.push_back(packet.TriggerTimestamp());
										
					++triggerCount;
				}
				else if(packet.IsRequestTime() && dev->endTimeLow == packet.Low() && dev->endTimeHigh == packet.High())
				{
					hbts.push_back(packet.Timestamp() >> 12);
					endMarker = true;
					++timeCount;
				}
				else
				{
					++otherCount;
					++otherHeaderCounts[packet.Header()];
				}
			}
		}
				
		if( timeout    ) throw runtime_error("Error: Timeout exhausting buffer");
		if( linkProblem) throw runtime_error("Error: Unstable link"            );
		if(!beginMarker) throw runtime_error("Error: Missing begin marker"     );
		if(!endMarker  ) throw runtime_error("Error: Missing end marker"       );
		
		
		// Sort on time
		sort(tdcs.begin(), tdcs.end());
		sort(hbts.begin(), hbts.end());
		
		for(auto& hits : hitsPerPixel)
			sort(hits.second.begin(), hits.second.end());
			
			
		// Fill the trees
		map<int, int> iterators;
		
		for(int pix : pixelMap)
			iterators[pix] = 0;

		dev->delay = delay;
		
		// Store the TDC timestamps and associate them to hits
		for(const auto& tdc : tdcs)
		{  
			dev->tdcTimestamp = tdc;
			dev->tdcTree->Fill();
			
			// for(int iPix=0; iPix<nPixels; ++iPix)
			for(int pix : pixelMap)
			{
				// auto& hits = hitsPerPixel[iPix];
				auto& hits = hitsPerPixel[pix];
				const int nHits = hits.size();
				
				// for(auto& iHit=iterators[iPix]; iHit<nHits; ++iHit)
				for(auto& iHit=iterators[pix]; iHit<nHits; ++iHit)
				{
					const auto toa = 6*get<0>(hits[iHit]);
					
					// dev->resVal = toa - tdc;
					// dev->resTree->Fill();
					
					dev->resHis->Fill((toa - tdc)*25./96);
					
					if(toa < tdc + tdcOffset)
						continue;
					
					if(toa >= tdc + tdcOffset + tdcWindow)
						break;
					
					get<2>(hits[iHit]) = tdc;
				}
			}
		}
				
		// for(int col=colPhase; col<256; col+=colSpacing)
		// for(int row=rowPhase; row<256; row+=rowSpacing)
		for(int pix : pixelMap)
		{
			dev->hitCol = pix >> 8 & 0xFF;
			dev->hitRow = pix >> 0 & 0xFF;
			// auto& hits = hitsPerPixel[(dev->hitCol/colSpacing)*256/rowSpacing + dev->hitRow/rowSpacing];
			auto& hits = hitsPerPixel[pix];
			
			
			
			// Take out the phase correction and use the TDC timestamp to align the 25 ns part of the toa
			int64_t firstToa = 0; // Will contain the the first toa up to a multiple of 25 ns
			int64_t firstTdc = 0;
			vector<int64_t> toas;			
			
			for(auto& hit : hits)
			{
				auto& toa = get<0>(hit);
				const auto& tdc = get<2>(hit);
					
				if(!tdc)
					continue;
				
				toa -= stpx3dt::Packet::PhaseCorrection(dev->hitCol);
				
				if(!firstToa)
				{
					firstToa = toa - (toa & 0xF);
					firstTdc = tdc;
				}
				
				toa -= firstToa + int(round(double(tdc - firstTdc)/(tpPeriod*96)))*tpPeriod*16;
				
				toas.emplace_back(toa);
			}
			
			if(toas.empty())
				continue;
			
			
			// Take the toas relative to the 25 ns median and keep only the best toa when there are multiuple hits per TDC
			const int64_t baseToa = toas[toas.size()>>1] - (toas[toas.size()>>1] & 0xF); // Median 25 ns part of toas
			const int64_t medianToa = toas[toas.size()>>1] - baseToa; // Median 25ns/16 part of toas
			tuple<int64_t, int16_t, int64_t>* prevHit = nullptr;
			
			toas.clear();
			
			for(auto& hit : hits)
			{
				auto& toa = get<0>(hit);
				auto& tdc = get<2>(hit);
				
				if(!tdc)
					continue;
				
				toa -= baseToa;
				
				// Multiple hits for this tdc Keep the one closest to the median toa
				if(prevHit && tdc == get<2>(*prevHit) && abs(toa - medianToa) > abs(get<0>(*prevHit) - medianToa))
				{
					tdc = 0; // Flag to ignore hit
					continue;
				}
				
				prevHit = &hit;
			}
			
			
			// Count the unique toa values and store in tree
			bool first = true;
			unsigned count = 0;
			
			sort(hits.begin(), hits.end());
			
			dev->hitCount = 0;
			
			for(const auto& hit : hits)
			{
				const auto& toa = get<0>(hit);
				const auto& tot = get<1>(hit);
				const auto& tdc = get<2>(hit);
				
				if(!tdc)
					continue;
				
				dev->totHis->Fill(tot);
				dev->hitmap->Fill(dev->hitCol, dev->hitRow);
				++count;
				
				if(first)
				{
					dev->hitToa = toa;
					first = false;
				}
				
				if(toa == dev->hitToa)
				{
					++dev->hitCount;
					continue;
				}
				
				dev->hitTree->Fill();
				
				dev->hitToa = toa;
				dev->hitCount = 1;
			}
			dev->hitTree->Fill(); 
			
			dev->cntHis->Fill(count);
		}
	}
	
}

int GetSendPackets()
{
	int attempts = 0;
	int count = 0;
	
	for(; attempts<nSpidrAttempts ; ++attempts)
	{
		if(attempts) this_thread::sleep_for(chrono::milliseconds(100));
		if(ctrl->getDataPacketCounter(&count)) break;
		Error("Error getting packet counter: " + ctrl->errorString());
	}
	
	if(attempts == nSpidrAttempts)
		throw runtime_error("Too many attempts");
	
	return count;
}

int GetReceivedPackets()
{
	int total=0;
	
	for(const auto& dev : activeDevices)
		total += dev->daq->packetsReceivedCount();
	
	return total;
}

string MakeFilenameTimestamp()
{
	char fileTimestamp[256]; 
	auto tNow    =  time(nullptr);
	auto tStruct = *localtime(&tNow);
	strftime(fileTimestamp, sizeof(fileTimestamp), "%y%m%d-%H%M%S%z", &tStruct);
	
	return fileTimestamp;
}

void FlushSamples()
{
	constexpr uint64_t linkProblemPacket = 0x780278027802aaaaull;
	
	for(auto& dev : activeDevices)
	{
		const auto timeLimit = chrono::steady_clock::now() + chrono::milliseconds(5000);
		bool timeout         = false;
		bool linkProblem     = false;
		uint64_t packetValue = 0;
		
		while(!linkProblem && !dev->daq->bufferEmpty() && dev->daq->getSample(0x1000000, 100))
		{
			if(chrono::steady_clock::now() > timeLimit)
			{
				timeout = true;
				break;
			}
			while((packetValue = dev->daq->nextPacket()))
				if(packetValue == linkProblemPacket)
				{
					linkProblem = true;
					break;
				}
		}
		
		if(timeout    ) throw runtime_error("Error: Timeout flushing buffer");
		if(linkProblem) throw runtime_error("Error: Unstable link"          );
	}
}

void InitPulseGen()
{
	tpdc = new PulseDelayController(pulGenAddr);
	// tpFrequency = tpdc->TriggerFrequency(); // I used 1.22 kHz (40 MHz/2^15). This measurement is not always accurate.
	
	
	cerr << "Expected test pulse frequency: " << tpFrequency << " Hz" << endl;
	cerr << "Measured test pulse frequency: " << tpdc->TriggerFrequency() << " Hz" << endl;
	
	// if(fabs(tpFrequency - 40.e6/tpPeriod) > tpFrequencyTolerance)
	// 	throw PulseDelayController::Error("Error: Unexpected test pulse frequency");
	
	tpdc->SetWidth(tpDigital? digPulseWidth : chrono::duration<double>(0.5/tpFrequency), tpChannel);
	tpdc->SetWidth(tdcPulseWidth, tdcChannel);
	
	if(!tpDigital && polarity < 0)
		tpdc->SetInvert(true, tpChannel);
}

void signalHandler(int signum)
{
	::signum = signum;
}

void SetSignalHandlers(bool set)
{
	static sighandler_t prevSigintHandler;
	static sighandler_t prevSigtermHandler = nullptr;
	
	if(set)
	{
		prevSigintHandler  = signal(SIGINT , signalHandler);
		prevSigtermHandler = signal(SIGTERM, signalHandler);
	}
	else
	{
		signal(SIGINT , prevSigintHandler );
		signal(SIGTERM, prevSigtermHandler);
	}
	
}

void SpidrPowerCycle()
{
	ConradController controller(conradComport.c_str());

	if(!controller.initialize())
	{
		cerr << "Error connecting to relais controller." << endl;
		return;
	}
	
	controller.setChannel(conradChannel, true);
	this_thread::sleep_for(chrono::milliseconds(1000));
	controller.setChannel(conradChannel, false);
	this_thread::sleep_for(chrono::milliseconds(1000));
}

template<class T> T FromString(const char* arg)
{
	stringstream ss;
	T value;
	
	if(!(ss << arg && ss >> value))
	{
		cerr << "Error parsing '" << arg << "' into a " << typeid(T).name() << endl;
		exit(1);
	}
	
	return value;
}
