#ifndef PULSE_DELAY_CONTROLLER_H
#define PULSE_DELAY_CONTROLLER_H

#include <vector>
#include <string>
#include <sstream>
#include <thread>
#include <chrono>
#include <exception>

#include "visa.h"

class PulseDelayController
{
	const static int timeOut = 5000; // 5 second time out
public:
	class Error : public std::exception
	{
	public:
		Error(const std::string& msg):exception(),message(msg){}
		
		const char* what() const throw(){ return message.c_str(); }
		
	protected:
		std::string message;
	};
	
	class CriticalError : public std::exception
	{
	public:
		CriticalError(const std::string& msg):exception(),message(msg){}
		
		const char* what() const throw(){ return message.c_str(); }
		
	protected:
		std::string message;
	};

	PulseDelayController(const std::string& resourceName):
	defRm(),instr()
	{
		if(viOpenDefaultRM(&defRm) != VI_SUCCESS) throw CriticalError("Error: Could not open a session to the VISA Resource Manager");
		if(viOpen(defRm, (ViRsrc) resourceName.c_str(), VI_NULL, timeOut, &instr) != VI_SUCCESS) throw CriticalError("Could not open a session to " + resourceName);
		viSetAttribute(instr, VI_ATTR_TMO_VALUE, timeOut);
		
		
		std::istringstream iss(SendCommand("*IDN?"));
		std::vector<std::string> fields(1);
		
		while(getline(iss, fields.back(), ',')) fields.emplace_back();
		fields.pop_back();
		
		std::cerr << "Connected to " << fields.at(1) << " at " << resourceName << std::endl;
		
		// Put the pulse generator into its default state
		SendCommand("*rst");
		SendCommand("*wai");
		
		Stop();
		
		// Set up the trigger generated from counting down the SPIDR clock out
		SendCommand("trigger:mode single");
		SendCommand("trigger:source external");
		SendCommand("trigger:slope falling");
		SendCommand("trigger:threshold -1.5V");
		
		// Set up output 1 for the external test pulse
		SendCommand("source1:pulse1:width 50ns");
		SendCommand("source1:pulse1:delay 0ps");
		SendCommand("source1:load:compensation off");
		SendCommand("source1:load:impedance 50");
		SendCommand("source1:volt:low 0V");
		SendCommand("source1:volt:high 5V");
		
		
		// Set up output 2 for the SPIDR TDC
		SendCommand("source2:pulse1:width 50ns");
		SendCommand("source2:pulse1:delay 0ps");
		SendCommand("source2:load:compensation off");
		SendCommand("source2:load:impedance 50");
		SendCommand("source2:volt:low 0V");
		SendCommand("source2:volt:high 2V");
		
		
		// Enable the outputs
		SendCommand("output1:state on");
		SendCommand("output2:state on");
		
		SendCommand("*wai");
			
		
		auto response = SendCommand("system:error?");
		
		if(SendCommand("system:error?") != "Error: 0, No error")
			throw Error("Error setting up the device: " + response);
			
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
	
	~PulseDelayController()
	{
		viClose(instr);
		viClose(defRm);
	}
	
	int  Status(){ return stoi(SendCommand("pulsegencontrol:status?")); }
	
	void Start(bool block=true){ SendCommand("pulsegencontrol:start"); if(block) WaitForStatus(1); }
	void Stop (bool block=true){ SendCommand("pulsegencontrol:stop" ); if(block) WaitForStatus(0); }
	
	void SetWidth(int width, int channel=0) // Width in picoseconds. 50 ns minimum
	{
		const std::string str = std::to_string(width);
		
		// int running = Status();
		
		// if(running)
		// 	Stop();
			
		// std::cerr << str << std::endl;
			
		if(!channel || channel==1) SendCommand("source1:pulse1:width "+str+"ps");
		if(!channel || channel==2) SendCommand("source2:pulse1:width "+str+"ps");
		SendCommand("*wai");
		// std::cerr << SendCommand("source1:pulse1:width?") << std::endl;
		// std::cerr << SendCommand("source2:pulse1:width?") << std::endl;
		
		// if(running)
		// 	Start();
	}
	
	void SetDelay(int delay, int channel=0) // Set the delay in picoseconds
	{
		const std::string str = std::to_string(delay);
		
		// int running = Status();
		
		// if(running)
		// 	Stop();
			
		if(!channel || channel==1) SendCommand("source1:pulse1:delay "+str+"ps");
		if(!channel || channel==2) SendCommand("source2:pulse1:delay "+str+"ps");
		SendCommand("*wai");
		
		// if(running)
		// 	Start();
	}
	
	template<class Rep, class Per>
	void SetWidth(const std::chrono::duration<Rep,Per>& width, int channel=0) // Set the delay in picoseconds
	{
		typedef std::ratio_divide<Per, std::pico> to_ps;
		int picoseconds = int(round(width.count()*to_ps::num*1./to_ps::den));
		
		SetWidth(picoseconds, channel);
	}
	
	template<class Rep, class Per>
	void SetDelay(const std::chrono::duration<Rep,Per>& delay, int channel=0) // Set the delay in picoseconds
	{
		typedef std::ratio_divide<Per, std::pico> to_ps;
		int picoseconds = int(round(delay.count()*to_ps::num*1./to_ps::den));
		
		SetDelay(picoseconds, channel);
	}
	
	double TriggerFrequency()
	{
		const std::string response = SendCommand("trigger:frequency?");
		std::istringstream iss(response);
		double value;
		std::string unit;
		
		if(!(iss >> value ))
			throw Error("Could not parse trigger frequency: " + response);
		
		if(iss>>unit && !unit.empty())
		{
			     if(unit == "K") value *= 1.e3;
			else if(unit == "M") value *= 1.e6;
			else if(unit == "G") value *= 1.e9;
			else throw Error("Could not parse trigger frequency: " + response);
		}
		
		return value;
	}
	
	void SetInvert(bool invert, int channel=0)
	{
		int running = Status();
		std::string arg = invert? "ON" : "OFF";
		
		if(running)
			Stop();
			
		if(!channel || channel==1) SendCommand("source1:invert " + arg);
		if(!channel || channel==2) SendCommand("source2:invert " + arg);
		SendCommand("*wai");
		
		if(running)
			Start();
	}
	
protected:
	ViSession defRm, instr;
	
	// bool Error(const std::string& errMsg)
	// {
	// 	std::cerr << errMsg << std::endl; 
	// 	exit(1); 
	// 	return false;
	// }
	
	std::string SendCommand(const std::string& command)
	{
		const int bufSize = 256;
		char buffer[bufSize] = {0};
		unsigned int count = 0;
		int status = viWrite(instr, (ViBuf) command.c_str(), command.length(), &count);
		
		if(status != VI_SUCCESS)
		{
			viStatusDesc(instr, status, buffer);
			Error("Error Writing to instrument: " + std::string(buffer));
		}
		
		status = viRead(instr, (ViBuf) buffer, bufSize, &count);
		
		if(status != VI_SUCCESS)
		{
			viStatusDesc(instr, status, buffer);
			Error("Error Reading from instrument: " + std::string(buffer));
		}
		
		buffer[count-1] = 0; // Remove trailing newline
		
		return buffer;
	}
	
	void WaitForStatus(int status)
	{
		namespace cn = std::chrono;
		auto begin = cn::steady_clock::now();
		
		while(true)
		{
			const int duration = cn::duration_cast<cn::milliseconds>(cn::steady_clock::now() - begin).count();
			
			if(duration > timeOut)
				Error("Timeout waiting for status change");
			
			if(status == Status())
				return;
			
			std::this_thread::sleep_for(cn::milliseconds(10));
		}
	}
};

#endif //PULSE_DELAY_CONTROLLER_H
