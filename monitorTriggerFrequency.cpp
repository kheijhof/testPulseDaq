#include <iostream>
#include <thread>
#include <chrono>

#include "PulseDelayController.h"

using namespace std;

int main(int argc, char** argv)
{
	const string pulGenAddr = "TCPIP::192.168.1.4::INSTR";
	PulseDelayController tpdc(pulGenAddr);
	
	cerr << "Measured test pulse frequency [Hz]: " << endl;
	
	while(true)
	{
		cerr << tpdc.TriggerFrequency() << endl;
		this_thread::sleep_for(chrono::milliseconds(1000));
	}
	
	return 0;
}