#include <iostream>
#include <thread>
#include <chrono>

#include "ConradController.h"

using namespace std;

int main(int argc, char** argv)
{
	const string conradComport = "/dev/ttyUSB0";
	const int    conradChannel = 1;

	ConradController controller(conradComport.c_str());

	if(!controller.initialize())
	{
		cerr << "Error connecting to relais controller." << endl;
		return 1;
	}
	
	controller.setChannel(conradChannel, true);
	this_thread::sleep_for(chrono::milliseconds(1000));
	controller.setChannel(conradChannel, false);
	
	return 0;
}