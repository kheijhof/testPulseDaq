#include <iostream>
#include <iomanip>
#include <cmath>
#include <set>
#include <unordered_set>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm> 
#include <fstream> 
#include <sstream>
#include <numeric>
#include <iterator>
#include <map>
#include <unordered_map>
#include <chrono>
#include <ctime>
#include <limits>
#include <exception>
#include <csignal>

#include <thread>
#include <mutex>

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TGraphErrors.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "TH1D.h"
#include "TMacro.h"

#include "SpidrController.h"
#include "SpidrDaq.h"
#include "tpx3defs.h"
#include "tpx3dacsdescr.h"

#include "PulseDelayController.h"
#include "DacCalibrationReader.h"
#include "ProgressPrinter.h"

#include "stpx3dt/Packet.h"
#include "stpx3dt/PacketStream.h"
#include "stpx3dt/PixelSorter.h"
#include "stpx3dt/SortedPacketStream.h"

using namespace std;

// Some paths
const string configsDir = "/localstore/TPX3/CONFIGS";

// Timpix3/SPIDR configuration
const int    spidrAddr[] = {192,168,1,10};
const int    spidrPort   = 50000;
const bool   extClk      = false;

const int    nSamples      =   10  ;
const int    nMeasurements =  100  ;
const double refVoltage    =    1.5;
const int    nAdcValues    = 4096  ;

const vector<int> devices = {0}; // devices to measure
const int links = 0x0F; // Links to enable

const int polarity    = -1;

const int genCfg = (polarity<0? TPX3_POLARITY_EMIN : TPX3_POLARITY_HPLUS) | 
                   TPX3_ACQMODE_TOA_TOT  | 
                   TPX3_GRAYCOUNT_ENA    | 
                   TPX3_TESTPULSE_ENA    | 
                   TPX3_FASTLO_ENA       | 
                   TPX3_SELECTTP_EXT_INT ;

const int pllCfg = TPX3_PLL_RUN          |
                   TPX3_VCNTRL_PLL       |
                   TPX3_DUALEDGE_CLK     |
                   TPX3_PHASESHIFT_DIV_8 |
                   TPX3_PHASESHIFT_NR_16 ;

const int    hdmiSwitch  = 0xFFFFF6;

const vector<int> dacCodes = {/*TPX3_VTHRESH_FINE, TPX3_VTHRESH_COARSE,*/ TPX3_VTP_COARSE, TPX3_VTP_FINE};
const bool scanThreshold = true; // Coarse and fine thresholds DACS are special

struct DeviceStruct
{	
	int index;
	string id;
	
	array<int, TPX3_DAC_COUNT_TO_SET> dacValues;
};

string ErrStatString(int errStat);
string FormatDeviceId(int device_id_int);

SpidrController* ctrl = nullptr; // Spidr connection
vector<DeviceStruct*> activeDevices; // To store the active device variables

void InitTpx3(); // Connect to the spidr and configure
void T0Sync();

template<class T, int N> T GetArgument(int argc, char** argv);
tuple<double, double> MeasureDacVoltage(int devIndex);

int main(int argc, char** argv)
{
	// Initialise
	try
	{
		InitTpx3();
	}
	catch(std::exception& e)
	{
		cerr << e.what() << endl;
		return 1;
	}
	
	
	int nSteps = 0;
	
	for(const auto& dacCode : dacCodes)
		nSteps += 1 << TPX3_DAC_TABLE[dacCode  -1].bits;
	
	if(scanThreshold)
		nSteps += (1 << TPX3_DAC_TABLE[TPX3_VTHRESH_COARSE-1].bits)*(1 << TPX3_DAC_TABLE[TPX3_VTHRESH_FINE-1].bits);
	
	nSteps *= activeDevices.size();
		
	ProgressPrinter progress("Measuring", "Completed in", nSteps);
	
	progress.Start();
		
	// Do the scan
	for(auto& dev : activeDevices)
	{
		const string filename = "dacCalibrations/dacCalibration-" + dev->id + ".root"; 
		auto file = new TFile(filename.c_str(), "RECREATE");
		auto tree = new TTree("dacCalibration", "");
		int code, value;
		double voltage, error;
		
		if(!file->IsOpen()) throw runtime_error("Cannot open "+filename+" for writing");
		
		tree->Branch("code"   , &code   , "code/I"   );
		tree->Branch("value"  , &value  , "value/I"  );
		tree->Branch("voltage", &voltage, "voltage/D");
		tree->Branch("error"  , &error  , "error/D"  );
		
		for(const auto& dacCode : dacCodes)
		{
			const int maxDacVal = 1 << TPX3_DAC_TABLE[dacCode  -1].bits;
			vector<double> dacVals, adcVals, adcErrs;
			
			code = dacCode;
			
			if(!ctrl->setSenseDac(dev->index, dacCode)) throw runtime_error("Error setting sense DAC: " + ctrl->errorString());
			
			for(value=0; value<maxDacVal; ++value, ++progress)
			{
				if(!ctrl->setDac(dev->index, dacCode, value)) throw runtime_error("Error setting DAC: " + ctrl->errorString());
				
				this_thread::sleep_for(chrono::microseconds(200)); // Let the dac stabilise
				
				tie(voltage, error) = MeasureDacVoltage(dev->index);
				
				dacVals.push_back(value  );
				adcVals.push_back(voltage);
				adcErrs.push_back(error  );
				
				tree->Fill();
			}
			
			// Set the DAC back to its initial value
			if(!ctrl->setDac(dev->index, dacCode, dev->dacValues[dacCode-1])) throw runtime_error("Error setting DAC: " + ctrl->errorString());
			
			// Store a graph for this DAC
			string dacName = TPX3_DAC_TABLE[dacCode-1].name;
			auto graph = new TGraphErrors(dacVals.size(), dacVals.data(), adcVals.data(), 0, adcErrs.data());
			
			graph->SetTitle((dacName+";DAC value; Voltage [V]").c_str());
			graph->Write(dacName.c_str());
		}
		
		tree->AutoSave();
		
		if(scanThreshold) // Requires a 2D scan of the coarse and fine threshold DACS
		{
			auto tree = new TTree("thrCalibration", "");
			int coarse, fine;
			const int maxCoarse = 1 << TPX3_DAC_TABLE[TPX3_VTHRESH_COARSE-1].bits;
			const int maxFine   = 1 << TPX3_DAC_TABLE[TPX3_VTHRESH_FINE  -1].bits;
			
			tree->Branch("coarse" , &coarse , "coarse/I" );
			tree->Branch("fine"   , &fine   , "fine/I"   );
			tree->Branch("voltage", &voltage, "voltage/D");
			tree->Branch("error"  , &error  , "error/D"  );
			
			if(!ctrl->setSenseDac(dev->index, TPX3_VTHRESH_FINE)) throw runtime_error("Error setting sense DAC: " + ctrl->errorString());
			
			for(coarse=0; coarse<maxCoarse; ++coarse)
			{
				vector<double> dacVals, adcVals, adcErrs;
				
				if(!ctrl->setDac(dev->index, TPX3_VTHRESH_COARSE, coarse)) throw runtime_error("Error setting DAC: " + ctrl->errorString());
				this_thread::sleep_for(chrono::microseconds(200)); // Let the dac stabilise
				
				for(fine=0; fine<maxFine; ++fine, ++progress)
				{
					if(!ctrl->setDac(dev->index, TPX3_VTHRESH_FINE, fine)) throw runtime_error("Error setting DAC: " + ctrl->errorString());
					
					this_thread::sleep_for(chrono::microseconds(200)); // Let the dac stabilise
					
					tie(voltage, error) = MeasureDacVoltage(dev->index);
					
					dacVals.push_back(fine   );
					adcVals.push_back(voltage);
					adcErrs.push_back(error  );
				
					tree->Fill();
				}
				
				// Store a graph for this DAC
				const string dacNameCoarse = TPX3_DAC_TABLE[TPX3_VTHRESH_COARSE-1].name;
				const string dacNameFine   = TPX3_DAC_TABLE[TPX3_VTHRESH_FINE  -1].name;
				auto graph = new TGraphErrors(dacVals.size(), dacVals.data(), adcVals.data(), 0, adcErrs.data());
				
				graph->SetTitle((dacNameFine+", "+dacNameCoarse+" = "+to_string(coarse)+";DAC value; Voltage [V]").c_str());
				graph->Write((dacNameFine+to_string(coarse)).c_str());
			}
			
			tree->AutoSave();
		}
		
		file->Write(0, TObject::kOverwrite);
		file->Close();
	
		// delete tree; // Deleted by ROOT
		delete file;
	}
	
	progress.Stop();
	
	delete ctrl;
	
	return 0;
}

string ErrStatString(int errStat)
{
	const char* resetErrorsByte0[] = {
		"",
		"TPX3_ERR_SC_ILLEGAL", 
		"TPX3_ERR_SC_STATE", 
		"TPX3_ERR_SC_ERRSTATE", 
		"TPX3_ERR_SC_WORDS", 
		"TPX3_ERR_TX_TIMEOUT", 
		"TPX3_ERR_EMPTY", 
		"TPX3_ERR_NOTEMPTY", 
		"TPX3_ERR_FULL", 
		"TPX3_ERR_UNEXP_REPLY", 
		"TPX3_ERR_UNEXP_HDR"
	};

	const char* resetErrorsByte1[] = {
		"",
		"SPIDR_ERR_I2C_INIT",
		"SPIDR_ERR_GTX_INIT",
		"SPIDR_ERR_MAX6642_INIT",
		"SPIDR_ERR_INA219_0_INIT",
		"SPIDR_ERR_INA219_1_INIT",
		"SPIDR_ERR_I2C"
	};

	const char* resetErrorsByte3[] = {
		"",
		"STORE_ERR_TPX",
		"STORE_ERR_WRITE",
		"STORE_ERR_WRITE_CHECK",
		"STORE_ERR_READ",
		"STORE_ERR_UNMATCHED_ID",
		"STORE_ERR_NOFLASH"
	};

	ostringstream oss;
	int b0 = errStat >>  0 & 0xFF;
	int b1 = errStat >>  8 & 0xFF;
	int b3 = errStat >> 24 & 0xFF;
	
	if(b3) oss << resetErrorsByte3[b3];
	if(b1) oss << (oss.str().empty()?"":", ") << resetErrorsByte1[b1];
	if(b0) oss << (oss.str().empty()?"":", ") << resetErrorsByte0[b0];
	
	return oss.str();
}

string FormatDeviceId(int device_id_int)
{
    int wafer_number = device_id_int >> 8 & 0xFFF;
    int y_position   = device_id_int >> 4 & 0xF;
    int x_position   = device_id_int >> 0 & 0xF;
	ostringstream oss;
	
	oss << setfill('0');
	oss << 'W' << setw(4) << wafer_number << '_' << char('A' + x_position - 1) << setw(2) << y_position;
	
    return oss.str();
}

void InitTpx3()
{
	if(activeDevices.empty())
		for(const int devIndex : devices)
		{
			auto dev = new DeviceStruct;
			
			dev->index = devIndex;
			
			activeDevices.push_back(dev);
		}

	if(ctrl)
		delete ctrl;
	
	// Setup the SPIDR connection
	int errStat  = 0;
	
	ctrl = new SpidrController(spidrAddr[0], spidrAddr[1], spidrAddr[2], spidrAddr[3], spidrPort);
	
	if(!ctrl->isConnected()) throw runtime_error("Cannot connect to SPIDR: " +  ctrl->connectionErrString() + " (" +  ctrl->ipAddressString() + ", " + ctrl->connectionStateString() + ")");
	
	cerr << "Connected to SPIDR at " << ctrl->ipAddressString() << endl;
	
	// Check board ID
	int boardId=0;
	
	if(!ctrl->getChipboardId(&boardId)) throw runtime_error("Error getting board ID: " + ctrl->errorString());
	
	cerr << hex << showbase;
	
	if(boardId == -1)
	{
		const int oldId = boardId;
		boardId = 0x02000000;
		cerr << "Unknown board ID (" << oldId << "). Setting to " << boardId << endl;
		if(!ctrl->setChipboardId(boardId)) throw runtime_error("Error setting board ID: " + ctrl->errorString());
	}
	
	cerr << "Board ID: " << boardId << dec << endl;
	
	
	// Some initial configuration first
	int softVer  = 0;
	int firmVer  = 0;
	int nDevices = 0;
	
	if(!ctrl->setExtRefClk(extClk)) throw runtime_error("Error setting external clock: " + ctrl->errorString());
	
	if(!ctrl->reset(&errStat)) throw runtime_error("Error resetting SPIDR: " + ctrl->errorString());
	this_thread::sleep_for(chrono::milliseconds(100));
	
	
	if(!ctrl->resetDevice(0)) throw runtime_error("Error resetting device: " + ctrl->errorString());
	
	this_thread::sleep_for(chrono::milliseconds(100));
	
	
	if(!ctrl->reinitDevices()) throw runtime_error("Error reinitialising devices: " + ctrl->errorString());	
	
	this_thread::sleep_for(chrono::milliseconds(100));
	

	
	
	if(!ctrl->getSoftwVersion(&softVer)) throw runtime_error("Error getting software version: " + ctrl->errorString());
	if(!ctrl->getFirmwVersion(&firmVer)) throw runtime_error("Error getting firmware version: " + ctrl->errorString());
	if(!ctrl->getDeviceCount(&nDevices)) throw runtime_error("Error getting number of supported devices: " + ctrl->errorString());
	
	cerr << "Using " << (extClk?"ex":"in") << "ternal clock" << endl;
	
	cerr << "Reset error status: " << hex << errStat << dec << " (" << ErrStatString(errStat) << ")" << endl;
		
	cerr << hex << showbase
	     << "Class version:    " << ctrl->classVersion() << endl
	     << "Software version: " << softVer              << endl
	     << "Firmware version: " << firmVer              << endl
	     << dec << noshowbase
	     << "Supported devices: " << nDevices             << endl;
	
	this_thread::sleep_for(chrono::milliseconds(1));
	
	
	if(!ctrl->setSpidrReg(0x2B8, 0)) throw runtime_error("Error enabling TDC: " + ctrl->errorString()); // Enable TDC
	if(!ctrl->setSpidrReg(0x810, hdmiSwitch)) throw runtime_error("Error setting HMDI: " + ctrl->errorString()); // Configure HDMI
	if(!ctrl->setDecodersEna(true)) throw runtime_error("Error enabling decoder: " + ctrl->errorString()); // Let the fpga do the LFSR/gray decoding
	
	
	// Check if the devies are active and make daq objects
	cerr << "----------------------------------" << endl
		 << " Device           Links            " << endl
		 << " Num  Chip ID     Enabled  Locked " << endl;
	
	for(auto& dev : activeDevices)
	{
		// DeviceStruct* dev = nullptr;
		// int status = 0;
		int enabledMask = 0;
		int lockedMask  = 0;
		int devId;
		
	
		if(!ctrl->setOutputMask(dev->index, links)) throw runtime_error("Error setting Timepix3 output links: " + ctrl->errorString());
		if(!ctrl->setSpidrReg(0x300 + (dev->index<<2), (~links)&0xFF)) throw runtime_error("Error setting SPIDR3 links: " + ctrl->errorString());
		if(!ctrl->getLinkStatus(dev->index, &enabledMask, &lockedMask)) throw runtime_error("Error getting link status: " + ctrl->errorString());
		if(!ctrl->getDeviceId(dev->index, &devId)) throw runtime_error("Error getting device id: " + ctrl->errorString());
		
		dev->id = FormatDeviceId(devId);
		
		
		// Print some device info
		ostringstream oss;
		
		oss << setw( 4) << dev->index
		    << setw(11) << dev->id
		    << hex
		    << setw( 8) << "0x" << setfill('0') << setw(2) << enabledMask << setfill(' ')
		    << setw( 6) << "0x" << setfill('0') << setw(2) << lockedMask << setfill(' ');
		
		cerr << oss.str() << endl;
		
		if(!(enabledMask &  lockedMask)) throw runtime_error("Device "+to_string(dev->index)+" is not active.");
		if(  enabledMask != lockedMask ) throw runtime_error("Device "+to_string(dev->index)+" has missing links.");
	}
	
	cerr << "----------------------------------" << endl;
	
	
	// Configure the devices
	for(auto& dev : activeDevices)
	{
		// Configure the DACS
		const string dacFilename = configsDir + "/" + dev->id + "_dacs.txt";
		ifstream dacIfs(dacFilename);
		
		if(!ctrl->setDacsDflt(dev->index)) throw runtime_error("Error setting default DAC values for device " + to_string(dev->index) + ": " + ctrl->errorString()); // First set the default values
		
		if(!dacIfs) // Cannot open the dac file, use default values
			cerr << "Cannot open DAC configuration file " << dacFilename << ". Using default values. " << endl;
		else
		{
			string line;
			
			cerr << "Setting DAC values of " << dev->id << " according to " << dacFilename << endl;
			
			while(dacIfs && getline(dacIfs, line))
			{
				int code, value;
				
				if(line.front() == '#') continue; // Skip lines beginning with #
				
				if(!(istringstream(line) >> code >> value)) throw runtime_error("Error parsing DAC configuration file.");
				if(!ctrl->setDac(dev->index, code, value)) throw runtime_error("Error setting DAC: " + ctrl->errorString());
				dev->dacValues[code-1] = value;
			}
			
			dacIfs.close();
		}
		
		
		// Configure the pixels
		const string pixFilename = configsDir + "/" + dev->id + "_trimdacs.txt";
		ifstream pixIfs(pixFilename);
		
		if(!ctrl->resetPixels(dev->index)) throw runtime_error("Error resetting pixel configuration of device " + to_string(dev->index) + ": " + ctrl->errorString()); // First reset the pixel configuration on the chip.
		if(!ctrl->selectPixelConfig(dev->index)==dev->index) throw runtime_error("Error selecting pixel configuration " + to_string(dev->index) + ": " + ctrl->errorString());// Select the locally stored pixel configuration
		ctrl->resetPixelConfig(); // Clear the locally stored pixel configuration. (Cannot fail.)
		
		if(!pixIfs) // Cannot open pixel configuration file, use default values
			cerr << "Cannot open pixel configuration file " << pixFilename << ". Using default values. " << endl;
		else
		{
			string line;
			
			cerr << "Setting pixel configuration of " << dev->id << " according to " << pixFilename << endl;
			
			while(pixIfs && getline(pixIfs, line))
			{
				int col, row, threshold, mask;
				bool tp_ena; 
				
				if(line.front() == '#') continue; // Skip lines beginning with #
				if(!(istringstream(line) >> col >> row >> threshold >> mask >> tp_ena)) throw runtime_error("Error parsing pixel configuration file: " + line);
				
				if(!ctrl->setPixelThreshold(col, row, threshold)) throw runtime_error("Error setting pixel threshold: " + ctrl->errorString()); // Set the threshold tuning
				if(!ctrl->setPixelMask(col, row, mask)) throw runtime_error("Error setting pixel mask bit: " + ctrl->errorString()); // Set the pixel mask
				if(!ctrl->setPixelTestEna(col, row, false)) throw runtime_error("Error setting test pulse enable bit: " + ctrl->errorString()); // Set the pixel mask   // Set the test pulse bit
			}
			
			pixIfs.close();
			
			if(!ctrl->setPixelConfig(dev->index)) throw runtime_error("Error loading pixel configuration to device " + to_string(dev->index) + ": " + ctrl->errorString());
			
			// TODO check pixel configuration
		}
		
		
		// Set the configuration and masks
		int ethMask = 0;
		int cpuMask = 0;
		
		if(!ctrl->setGenConfig(dev->index, genCfg)) throw runtime_error("Error setting general configuration of device " + to_string(dev->index) + ": " + ctrl->errorString()); // Set the general configuration
		if(!ctrl->setPllConfig(dev->index, pllCfg)) throw runtime_error("Error setting PLL configuration of device " + to_string(dev->index) + ": " + ctrl->errorString()); // Set the PLL configuration
		if(!ctrl->getHeaderFilter(dev->index, &ethMask, &cpuMask)) throw runtime_error("Error getting header filters of device " + to_string(dev->index) + ": " + ctrl->errorString()); // First read the header filter masks
		if(!ctrl->setHeaderFilter(dev->index,   0xFFFF,  cpuMask)) throw runtime_error("Error setting header filters of device " + to_string(dev->index) + ": " + ctrl->errorString()); // Tell SPIDR to send all packets over ethernet.
	}
	
	if(!ctrl->resetCounters()) throw runtime_error("Error resetting counters.");
		
	T0Sync();
}

void T0Sync()
{
	// TODO implement retry
	if(!ctrl->setSpidrRegBit(0x290, 5, true )) throw runtime_error("Error setting t0 bit: " + ctrl->errorString());
	if(!ctrl->setSpidrRegBit(0x290, 5, false)) throw runtime_error("Error resetting t0 bit: " + ctrl->errorString());
}

template<class T, int N> T GetArgument(int argc, char** argv)
{
	stringstream ss;
	T value;
	
	if(N >= argc)
	{
		cerr << "Expecting more command line arguments" << endl;
		exit(1);
	}
	
	if(!(ss << argv[N] && ss >> value))
	{
		cerr << "Error parsing command line argument" << endl;
		exit(1);
	}
	
	return value;
}

tuple<double, double> MeasureDacVoltage(int devIndex)
{
	vector<double> values;
	
	for(int i=0; i<nMeasurements; ++i)
	{
		int adcVal = 0;
		
		if(!ctrl->getDacOut(devIndex, &adcVal, nSamples)) throw runtime_error("Error setting DAC: " + ctrl->errorString());
		
		values.push_back(adcVal*refVoltage/(nAdcValues - 1)/nSamples);
	}
	
	const double mean  = accumulate(values.begin(), values.end(), 0.)/values.size();
	const double error = sqrt(accumulate(values.begin(), values.end(), 0., [mean](const double& var, const double& val){ const double d=(val-mean); return var + d*d; })/((values.size() - 1)*values.size()));
	
	return make_tuple(mean, error);
}
